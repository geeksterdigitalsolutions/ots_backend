FROM node:10.16

ADD ./oracle/*.zip ./

ENV LD_LIBRARY_PATH="/opt/oracle/instantclient"
ENV OCI_HOME="/opt/oracle/instantclient"
ENV OCI_LIB_DIR="/opt/oracle/instantclient"
ENV OCI_INCLUDE_DIR="/opt/oracle/instantclient/sdk/include"
ENV OCI_VERSION=19

# install dependencies and extract binaries`
RUN apt-get update
RUN apt-get install -y unzip libaio1
RUN mkdir -p /opt/oracle
RUN unzip instantclient-basic-linux.x64-19.3.0.0.0dbru.zip -d /opt/oracle
RUN unzip instantclient-sdk-linux.x64-19.3.0.0.0dbru.zip -d /opt/oracle
RUN mv "${LD_LIBRARY_PATH}_19_3" $LD_LIBRARY_PATH
RUN ln -s "${LD_LIBRARY_PATH}/libclntsh.so.19.3 "${LD_LIBRARY_PATH}/libclntsh.so
RUN rm -f instantclient-*.zip
RUN apt-get remove -y unzip
RUN apt-get clean autoclean
RUN apt-get autoremove -y
RUN rm -rf /var/lib/{apt,dpkg,cache,log}

RUN echo "${LD_LIBRARY_PATH}" | tee -a /etc/ld.so.conf.d/oracle_instant_client.conf && ldconfig

RUN mkdir -p /app
WORKDIR /app
COPY . /app

COPY package.json /app/
RUN npm install -g fastify-cli
RUN npm install

EXPOSE 3000

CMD [ "npm", "run", "dev"]