'use strict'

const fp = require('fastify-plugin')
const moment = require('moment')

module.exports = fp(async (fastify, opts) => {

  const validateDateFormat = (date) => {
    if (!date || date === '-') return true
    return moment(date, 'DD/MM/YYYY').isValid()
  }

  fastify.decorate('validator', {
    frieghtImportValidate: async (items) => {
      if (items.length === 0) return ['No Item Found']
      if (items.length > 500)  return ['Import File are longer than 500 items']
      if (!Object.keys(items[0]).includes('baht')) return ['Wrong Import File Module']

      const errors = []
      let i = 1
      
      for (let item of items) {
        if (!item['delivery_no']) errors.push(`[List of Items ${i}]: No Delivery Order No.`)
        // if (!item['actual_eta']) return reply.code(400).send({ message: 'Actual ETA is null' })
        if (!item['so_no']) errors.push(`[List of Items ${i}]: No Sales Order No.`)
        if (!item['booking_no']) errors.push(`[List of Items ${i}]: No Booking No.`)
        if (item['remark'].length > 500) error.push(`[List of Items ${i}]: Remark is Longer`)
        const isHasFrieght = await fastify.service(fastify.oracle).isHasFrieght(item['booking_no'])
        if (!isHasFrieght) errors.push(`[List of Items ${i}]: No Booking No.`)

        i++
      }

      return errors
    },
    documentImportValidate: async (items) => {
      if (items.length === 0) return ['No Item Found']
      if (items.length > 500)  return ['Import File are longer than 500 items']
      if (!Object.keys(items[0]).includes('tracking_no')) return ['Wrong Import File Module']

      const errors = []
      let i = 1
      
      for(let item of items) {
        if (!item['do'])  errors.push(`[List of Items ${i}]: No Delivery Order No.`)
        if (!item['etd_port_of_loading']) errors.push(`[List of Items ${i}]: No ETD Port of loading`)
        if (!item['so']) errors.push(`[List of Items ${i}]: No Sales Order No.`)
        if (!item['booking_no']) errors.push(`[List of Items ${i}]: No Booking No.`)
        if (!(await fastify.service(fastify.oracle).isHasDocument(item['booking_no'], item['do']))) errors.push(`[List of Items ${i}]: Booking No. or Delivery No not match.`)
        if (!['', null, 'pending', 'completed'].includes(item['status_of_document'])) errors.push(`[List of Items ${i}]: Should be 'pending' or 'completed'`)
        if (!validateDateFormat(item['receive_lc'])) errors.push(`[List of Items ${i}]: Wrong receive_lc Type`)
        if (!validateDateFormat(item['lc_expiry'])) errors.push(`[List of Items ${i}]: Wrong lc_expiry Type`)
        if (!validateDateFormat(item['lds'])) errors.push(`[List of Items ${i}]: Wrong lds Type`)
        if (!validateDateFormat(item['period_of_presentation'])) errors.push(`[List of Items ${i}]: Wrong period_of_presentation Type`)
        if (!validateDateFormat(item['cbr_actual'])) errors.push(`[List of Items ${i}]: Wrong cbr_actual Type`)
        if (!validateDateFormat(item['si_actual'])) errors.push(`[List of Items ${i}]: Wrong si_actual Type`)
        if (!validateDateFormat(item['draft_bl_actual'])) errors.push(`[List of Items ${i}]: Wrong draft_bl_actual Type`)
        if (!validateDateFormat(item['confirm_bl_actual'])) errors.push(`[List of Items ${i}]: Wrong confirm_bl_actual Type`)
        if (!validateDateFormat(item['obl_actual'])) errors.push(`[List of Items ${i}]: Wrong obl_actual Type`)
        if (!validateDateFormat(item['apply_psyto_actual'])) errors.push(`[List of Items ${i}]: Wrong apply_psyto_actual Type`)
        if (!validateDateFormat(item['receive_psyto_actual'])) errors.push(`[List of Items ${i}]: Wrong receive_psyto_actual Type`)
        if (!validateDateFormat(item['apply_coci_chamber_actual'])) errors.push(`[List of Items ${i}]: Wrong apply_coci_chamber_actual Type`)
        if (!validateDateFormat(item['receive_coci_chamber_actual'])) errors.push(`[List of Items ${i}]: Wrong receive_coci_chamber_actual Type`)
        if (!validateDateFormat(item['apply_fta_form_actual'])) errors.push(`[List of Items ${i}]: Wrong apply_fta_form_actual Type`)
        if (!validateDateFormat(item['receive_fta_form_actual'])) errors.push(`[List of Items ${i}]: Wrong receive_fta_form_actual Type`)
        if (!validateDateFormat(item['be_actual'])) errors.push(`[List of Items ${i}]: Wrong be_actual Type`)
        if (!validateDateFormat(item['scan_sent_email_actual'])) errors.push(`[List of Items ${i}]: Wrong scan_sent_email_actual Type`)
        if (!validateDateFormat(item['complete_set_actual'])) errors.push(`[List of Items ${i}]: Wrong complete_set_actual Type`)
        if (!validateDateFormat(item['courier_date'])) errors.push(`[List of Items ${i}]: Wrong courier_date Type`)
        if (!validateDateFormat(item['sst_actual'])) errors.push(`[List of Items ${i}]: Wrong sst_actual Type`)
        if (!validateDateFormat(item['permit_actual'])) errors.push(`[List of Items ${i}]: Wrong permit_actual Type`)

        i++
      }

      return errors
    },
    truckImportValidate: async (items) => {
      if (items.length === 0) return ['No Item Found']
      if (items.length > 500)  return ['Import File are longer than 500 items']

      const errors = []
      let i = 1
      
      for (let item of items) {
        if (!item['field10']) errors.push(`[List of Items ${i}]: No Delivery Order No.`)
        if (!item['field9']) errors.push(`[List of Items ${i}]: No Commercial Invoice No.`)

        i++
      }

      return errors
    },
    containerImportValidate: async (items) => {
      if (items.length === 0) return ['No Item Found']
      if (items.length > 500)  return ['Import File are longer than 500 items']

      const errors = []
      let i = 1
      
      for(let item of items) {
        if (!item['field2'])  errors.push(`[List of Items ${i}]: No Booking No.`)

        const hasFrieght = await fastify.service(fastify.oracle).isHasFrieght(item['field2'])
        if (!hasFrieght) {
           errors.push(`[List of Items ${i}]: No Booking No.`)
        }

        i++
      }

      return errors
    },
    documentHyImportValidate: async (items) => {
      if (items.length === 0) return ['No Item Found']
      if (items.length > 500)  return ['Import File are longer than 500 items']

      const errors = []
      let i = 1
      
      for(let item of items) {
        if (!item['field1']) errors.push(`[List of Items ${i}]: No Sales Order No.`)
        if (!item['field4']) errors.push(`[List of Items ${i}]: No Item`)

        i++
      }

      return errors
    }
  })

})
