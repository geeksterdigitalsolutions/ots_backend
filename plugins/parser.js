'use strict'

const fp = require('fastify-plugin')
const moment = require('moment')

module.exports = fp(async function (fastify, opts) {

  fastify.decorate('parser', {
    dateFromCsvToOracleFormat: (date) => {
      return date && date != '-' ? moment(date, 'DD/MM/YYYY').toDate() : null
    },
    getDeliveryOnTimeFrieght: (ontime) => {
      // [if on time <=3 : ON TIME]; [if on time >3<=10 : DELAY 4-10 DAYS]; [if on time >10 : MORE THAN 10 DAYS DELAY]
      if (ontime == null) return 'FOLLOW UP'
      if (ontime <= 3) return 'ON TIME'
      else if (ontime > 3 && ontime <= 10) return 'DELAY 4-10 DAYS'
      else if (ontime > 10) return 'MORE THAN 10 DAYS DELAY'
    }
  })

})
