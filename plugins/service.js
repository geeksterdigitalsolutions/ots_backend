'use strict'

const fp = require('fastify-plugin')
const frieghtService = require('../services/frieght')
const documentService = require('../services/document')
const documentHyService = require('../services/document-hy')
const containerService = require('../services/container')
const truckService = require('../services/truck')
const truckDomesticService = require('../services/truck-domestic')
const routeService = require('../services/route')
const overseaService = require('../services/oversea')
const customerService = require('../services/customer')
const configService = require('../services/config')

module.exports = fp(async function (fastify, opts) {

  fastify.decorate('service', (oracleConnection) => ({
    ...frieghtService(oracleConnection),
    ...documentService(oracleConnection),
    ...documentHyService(oracleConnection),
    ...containerService(oracleConnection),
    ...truckService(oracleConnection),
    ...truckDomesticService(oracleConnection),
    ...routeService(oracleConnection),
    ...overseaService(oracleConnection),
    ...customerService(oracleConnection),
    ...configService(oracleConnection)
  }))
})
