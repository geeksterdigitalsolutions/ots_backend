'use strict'

const fp = require('fastify-plugin')
const moment = require('moment')

module.exports = fp(async function (fastify, opts) {

  fastify.decorate('routeUtils', {
    mapRoutePenang: (result) => {
      const actualBorder = result.LADEN_COME_BACK_TO_YARD_DATE
      result.START_PICKING_DATE = result.START_PICKING_DATE ? moment(result.START_PICKING_DATE).subtract(7, 'H') : null
      const data = [
        {
          name: 'PNP HY Factory',
          plan: result.ETD_PORT ? moment(result.ETD_PORT).subtract(3, 'd') : null,
          actual: result.START_PICKING_DATE,
          status: result.START_PICKING_DATE != null
        },
        {
          name: 'Border',
          plan: result.START_PICKING_DATE ? result.START_PICKING_DATE : moment(result.ETD_PORT).subtract(3, 'd'),
          actual: actualBorder,
          status: actualBorder != null
        },
        {
          name: 'Return to Port',
          plan: actualBorder ? moment(actualBorder).add(1, 'd') : moment(result.ETD_PORT).subtract(1, 'd'),
          actual: result.LADEN_CONTAINER_OUT_PORT_DATE,
          status: result.LADEN_CONTAINER_OUT_PORT_DATE != null
        },
        {
          name: 'POL',
          plan: result.ETD_PORT,
          actual: result.ACTUAL_ETD,
          status: result.ACTUAL_ETD != null
        },
        {
          name: 'POT',
          plan: result.ETD_PORT_OF_TRANSHIPMENT,
          actual: result.ACTUAL_ETD_POT,
          status: result.ACTUAL_ETD_POT != null
        },
        {
          name: 'POD',
          plan: result.ETA,
          actual: result.ACTUAL_ETA,
          status: result.ACTUAL_ETA != null
        }
      ]
      return data
    },
    mapRouteSongKhla: (result) => {
      const data = [
        {
          name: 'PNP HY Factory',
          plan: result.ETD_PORT ? moment(result.ETD_PORT).subtract(2, 'd') : null,
          actual: result.START_PICKING_DATE,
          status: result.START_PICKING_DATE != null
        },
        {
          name: 'Return to Port Songkla',
          plan: result.ETD_PORT ? moment(result.ETD_PORT).subtract(2, 'd') : null,
          actual: result.START_PICKING_DATE,
          status: result.START_PICKING_DATE != null
        },
        {
          name: 'POL Songkla',
          plan: result.ETD_PORT,
          actual: result.ACTUAL_ETD,
          status: result.ACTUAL_ETD != null
        },
        {
          name: 'POT',
          plan: result.ETD_PORT_OF_TRANSHIPMENT,
          actual: result.ACTUAL_ETD_POT,
          status: result.ACTUAL_ETD_POT != null
        },
        {
          name: 'POD',
          plan: result.ETA,
          actual: result.ACTUAL_ETA,
          status: result.ACTUAL_ETA != null
        }
      ]
      return data
    },
    mapRouteBangkokFromHY: (result) => {
      const data = [
        {
          name: 'PNP HY Factory',
          plan: moment(result.ETD_PORT).subtract(4, 'd'),
          actual: result.START_PICKING_DATE ? result.START_PICKING_DATE : null,
          status: result.START_PICKING_DATE != null
        },
        {
          name: 'CFS',
          plan: result.START_PICKING_DATE ? moment(result.START_PICKING_DATE).add(2, 'd') : moment(result.ETD_PORT).subtract(2, 'd'),
          actual: result.START_PICKING_DATE ? moment(result.START_PICKING_DATE).add(2, 'd') : null,
          status: result.START_PICKING_DATE != null
        },
        {
          name: 'Return to Port',
          plan: result.START_PICKING_DATE ? moment(result.START_PICKING_DATE).add(3, 'd') : moment(result.ETD_PORT).subtract(1, 'd'),
          actual: result.START_PICKING_DATE ? moment(result.START_PICKING_DATE).add(3, 'd') : null,
          status: result.START_PICKING_DATE != null
        },
        {
          name: 'POL',
          plan: result.ETD_PORT,
          actual: result.ACTUAL_ETD,
          status: result.ACTUAL_ETD != null
        },
        {
          name: 'POT',
          plan: result.ETD_PORT_OF_TRANSHIPMENT,
          actual: result.ACTUAL_ETD_POT,
          status: result.ACTUAL_ETD_POT != null
        },
        {
          name: 'POD',
          plan: result.ETA,
          actual: result.ACTUAL_ETA,
          status: result.ACTUAL_ETA != null
        }
      ]
      return data
    },
    mapRouteBangkokFromSK: (result) => {
      const data = [
        {
          name: 'PNP SK Factory',
          plan: result.ETD_PORT ? moment(result.ETD_PORT).subtract(2, 'd') : null,
          actual: null,
          status: false
        },
        {
          name: 'Return to Port',
          plan: result.ETD_PORT ? moment(result.ETD_PORT).subtract(1, 'd') : null,
          actual: null,
          status: false
        },
        {
          name: 'POL',
          plan: result.ETD_PORT,
          actual: result.ACTUAL_ETD,
          status: result.ACTUAL_ETD != null
        },
        {
          name: 'POT',
          plan: result.ETD_PORT_OF_TRANSHIPMENT,
          actual: result.ACTUAL_ETD_POT,
          status: result.ACTUAL_ETD_POT != null
        },
        {
          name: 'POD',
          plan: result.ETA,
          actual: result.ACTUAL_ETA,
          status: result.ACTUAL_ETA != null
        }
      ]
      return data
    },
    mapRouteExportPNPCustomer: (result) => {
      const borderPlanTime = result.CARGO_DEPARTURE_FROM_PNP_TIME_OUT ? moment(result.CARGO_DEPARTURE_FROM_PNP_TIME_OUT, 'hh:mm').add(1, 'h') : moment(result.ETD_PORT, 'hh:mm').add(1, 'h')
      const warehousePlanTime = result.CARGO_PNP_TO_CROSS_BORDER_TIME ? moment(result.CARGO_PNP_TO_CROSS_BORDER_TIME, 'hh:mm').add(1, 'h') : borderPlanTime.clone().add(1, 'h')

      const data = [
        {
          name: 'PNP HY Factory',
          plan: result.ETD_PORT ? moment(result.ETD_PORT) : null,
          actual: {
            date: result.CARGO_DEPARTURE_FROM_PNP_DATE_IN,
            time: result.CARGO_DEPARTURE_FROM_PNP_TIME_OUT
          },
          status: result.CARGO_DEPARTURE_FROM_PNP_DATE_IN !== null
        },
        {
          name: 'Border',
          plan: {
            date: result.CARGO_DEPARTURE_FROM_PNP_DATE_IN ? result.CARGO_DEPARTURE_FROM_PNP_DATE_IN : moment(result.ETD_PORT).format('DD/MM/YYYY'),
            time: borderPlanTime.format('HH:mm')
          },
          actual: {
            date: result.CARGO_PNP_TO_CROSS_BORDER_DATE,
            time: result.CARGO_PNP_TO_CROSS_BORDER_TIME
          },
          status: result.CARGO_PNP_TO_CROSS_BORDER_DATE !== null
        },
        {
          name: 'Warehouse',
          plan: {
            date: result.CARGO_PNP_TO_CROSS_BORDER_DATE,
            time: warehousePlanTime.format('HH:mm')
          },
          actual: {
            date: result.CARGO_BKHWH_TO_CUSTOMER_DATE,
            time: result.CARGO_BKHWH_TO_CUSTOMER_TIME
          },
          status: result.CARGO_BKHWH_TO_CUSTOMER_DATE != null
        },
        {
          name: 'Customer',
          plan: {
            date: result.ESTIMATE_ARRIVAL_DATE,
            time: result.ESTIMATE_ARRIVAL_TIME,
          },
          actual: moment().isAfter(moment(result.ESTIMATE_ARRIVAL_DATE, 'DD/MM/YYYY').format('YYYY-MM-DD')) ? 'Complete' : null,
          status: moment().isAfter(moment(result.ESTIMATE_ARRIVAL_DATE, 'DD/MM/YYYY').format('YYYY-MM-DD'))
        }
      ]
      return data
    },
    mapRouteExportPNPBorder: (result) => {
      const borderPlanTime = result.CARGO_DEPARTURE_FROM_PNP_TIME_OUT ? moment(result.CARGO_DEPARTURE_FROM_PNP_TIME_OUT, 'hh:mm').add(1, 'h') : moment(result.ETD_PORT, 'hh:mm').add(1, 'h')
      const warehousePlanTime = result.CARGO_PNP_TO_CROSS_BORDER_TIME ? moment(result.CARGO_PNP_TO_CROSS_BORDER_TIME, 'hh:mm').add(1, 'h') : borderPlanTime.clone().add(1, 'h')

      const data = [
        {
          name: 'PNP HY Factory',
          plan: result.ETD_PORT ? moment(result.ETD_PORT) : null,
          actual: {
            date: result.CARGO_DEPARTURE_FROM_PNP_DATE_IN,
            time: result.CARGO_DEPARTURE_FROM_PNP_TIME_OUT
          },
          status: result.CARGO_DEPARTURE_FROM_PNP_DATE_IN !== null
        },
        {
          name: 'Border',
          plan: {
            date: result.CARGO_DEPARTURE_FROM_PNP_DATE_IN ? result.CARGO_DEPARTURE_FROM_PNP_DATE_IN : moment(result.ETD_PORT).format('DD/MM/YYYY'),
            time: borderPlanTime.format('HH:mm')
          },
          actual: {
            date: result.CARGO_PNP_TO_CROSS_BORDER_DATE,
            time: result.CARGO_PNP_TO_CROSS_BORDER_TIME
          },
          status: result.CARGO_PNP_TO_CROSS_BORDER_DATE !== null
        },
        {
          name: 'Warehouse',
          plan: {
            date: result.CARGO_PNP_TO_CROSS_BORDER_DATE,
            time: warehousePlanTime.format('HH:mm')
          },
          actual: {
            date: result.CARGO_BKHWH_TO_CUSTOMER_DATE,
            time: result.CARGO_BKHWH_TO_CUSTOMER_TIME
          },
          status: result.CARGO_BKHWH_TO_CUSTOMER_DATE != null
        }
      ]
      return data
    },
    mapRouteExportPNPHYFactory: (result) => {
      const data = [
        {
          name: 'PNP HY Factory',
          plan: result.ETD_PORT ? moment(result.ETD_PORT) : null,
          actual: {
            date: result.CARGO_DEPARTURE_FROM_PNP_DATE_IN,
            time: result.CARGO_DEPARTURE_FROM_PNP_TIME_OUT
          },
          status: result.CARGO_DEPARTURE_FROM_PNP_DATE_IN !== null
        }
      ]
      return data
    },
    mapRouteExportPNPCLMVToSK: (result) => {
      const data = [
        {
          name: 'PNP HY Factory',
          plan: result.ETD_PORT ? moment(result.ETD_PORT).subtract(3, 'd') : null,
          actual: {
            date: result.CARGO_DEPARTURE_FROM_PNP_DATE_IN,
            time: result.CARGO_DEPARTURE_FROM_PNP_TIME_OUT
          },
          status: result.CARGO_DEPARTURE_FROM_PNP_DATE_IN !== null
        },
        {
          name: 'PNP SK Factory',
          plan: result.CARGO_DEPARTURE_FROM_PNP_DATE_IN ? result.CARGO_DEPARTURE_FROM_PNP_DATE_IN.add(3, 'd') : moment(result.ETD_PORT).subtract(1, 'd'),
          actual: {
            date: null,
            time: null
          },
          status: false
        }
      ]
      return data
    },
    mapRouteExportPNPCLMVToDAP: (result) => {
      const data = [
        {
          name: 'PNP HY Factory',
          plan: result.ETD_PORT ? moment(result.ETD_PORT).subtract(3, 'd') : null,
          actual: {
            date: result.CARGO_DEPARTURE_FROM_PNP_DATE_IN,
            time: result.CARGO_DEPARTURE_FROM_PNP_TIME_OUT
          },
          status: result.CARGO_DEPARTURE_FROM_PNP_DATE_IN !== null
        },
        {
          name: 'PNP SK Factory',
          plan: result.CARGO_DEPARTURE_FROM_PNP_DATE_IN ? result.CARGO_DEPARTURE_FROM_PNP_DATE_IN.add(3, 'd') : moment(result.ETD_PORT).subtract(1, 'd'),
          actual: {
            date: null,
            time: null
          },
          status: false
        },
        {
          name: 'Border',
          plan: null,
          actual: null,
          status: fakse
        },
      ]
      return data
    }
  })

})
