'use strict'

const fp = require('fastify-plugin')
const axios = require('axios')
const https = require('https')
const bcrypt = require('bcrypt')
const { promisify } = require('util')

const compare = promisify(bcrypt.compare)

module.exports = fp(async function (fastify, opts) {

  const getUserProfile = async (token) => await axios({
    method: 'get',
    url: `${process.env.MP_OAUTH_TOKEN_HOST}/user/profile`,
    headers: {Authorization: `Bearer ${token}`},
    httpsAgent: new https.Agent({  
      rejectUnauthorized: false
    }),
  }).then(response => response.data.success.data.user_info)

  fastify.decorate('oauth', {
    getAccessToken: async (credential) => {
      const { username, password } = credential

      return axios({
        method: 'post',
        url: `${process.env.MP_OAUTH_TOKEN_HOST}/oauth/token`,
        data: {
          grant_type: 'password',
          username,
          password: encodeURIComponent(password),
          client_id: process.env.MP_OAUTH_CLIENT_ID,
          client_secret: process.env.MP_OAUTH_SECRET,
        },
        transformRequest: [(data, headers) => Object.keys(data).map(key => `${key}=${data[key]}`).join('&')],
        httpsAgent: new https.Agent({  
          rejectUnauthorized: false
        }),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      })
      .then(response => ({ ...response.data, status: 200 }))
      .catch(e => {
        console.log(e)
        const { status } = e.response
        if (status === 401) return { status, message: 'Invalid credential' }
        if (status === 500) return { status, message: 'Problem AD server' }
      })
    },
    getUserProfile,
    verifyAccessToken: async (request, reply) => {
      const { headers, query } = request
      const { authorization } = headers
      
      if (authorization && authorization.startsWith('Bearer ')) {
        const [tokenType, tokenConten] = authorization.split(' ')
        try {
          const member = await getUserProfile(tokenConten)
          request.member = member

          return request
        } catch (e) {}
      } else if (query.token) {
        try {
          const member = await getUserProfile(query.token)
          request.member = member

          return request
        } catch (e) {
          console.log(e)
        }
      }
      
      return reply.code(401).send({ message: 'Unauthorized' })
    },
    refreshToken: async (refresh_token) => {
      return axios({
        method: 'post',
        url: `${process.env.MP_OAUTH_TOKEN_HOST}/oauth/token`,
        data: {
          grant_type: 'refresh_token',
          refresh_token
        },
        transformRequest: [(data, headers) => Object.keys(data).map(key => `${key}=${data[key]}`).join('&')],
        httpsAgent: new https.Agent({  
          rejectUnauthorized: false
        }),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      })
      .then(response => ({ ...response.data, status: 200 }))
      .catch(e => {
        console.log(e)
        const { status } = e.response
        if (status === 401) return { status, message: 'Refresh token not found' }
      })
    },
    customerLogin: async (data) => {
      const customers = await fastify.service(fastify.oracle).findByUsername(data.username)
      if (customers.length === 0) throw { message: 'User not found' }

      const customer = customers[0]
      const isMatch = await compare(data.password, customer.PASSWORD)
      
      if (!isMatch) throw { message: 'Password not match' }

      delete customer.PASSWORD

      customer.access_token = fastify.jwt.sign({ id: customer.ID, code: customer.CUSTOMER_CODE })

      return customer
    },
    verifyCustomerAccessToken: async (request, reply) => {
      try {
        await request.jwtVerify()
      } catch (err) {
        reply.send(err)
      }
    }
  })

})
