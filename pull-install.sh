#!/usr/bin/env bash

APP="ots_backend"
docker stop ${APP}
docker pull ninthz/ots_backend:dev
docker run -d --rm -p 3000:3000 --name ${APP} ninthz/ots_backend:dev