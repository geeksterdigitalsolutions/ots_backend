'use strict' 

module.exports = async (fastify, opts) => { 
  fastify.get('/shipment', async (request) => {
    const { query } = request
    return [
      {
        status: 'shipment',
        process: 4,
        complete: 0
      },
      {
        status: 'border',
        process: 2,
        complete: 4
      },
      {
        status: 'total truck',
        process: 30,
        complete: 20
      },
    ]
  })
}

module.exports.autoPrefix = '/summary'