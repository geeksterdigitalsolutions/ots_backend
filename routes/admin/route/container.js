'use strict' 
const moment = require('moment')

module.exports = async (fastify, opts) => { 

  fastify.get('/container-export-penang/:booking_no', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  },async (request) => {
    const { params } = request
    
    let result = await fastify.service(fastify.oracle).getRouteContainer(params.booking_no)
    if (result.length == 0) {
      return []  
    } else {
      result = result[0]
      if (['24', '25'].includes(result.BORDER_CODE)) {
        return fastify.routeUtils.mapRoutePenang(result)
      } else if (['18'].includes(result.BORDER_CODE)) {
        return fastify.routeUtils.mapRouteSongKhla(result)
      } else if (['01', '04'].includes(result.BORDER_CODE) && result.FAC_CODE === 'PNP2') {
        return fastify.routeUtils.mapRouteBangkokFromSK(result)
      } else if (['01', '04'].includes(result.BORDER_CODE)) {
        return fastify.routeUtils.mapRouteBangkokFromHY(result)
      } 
      return []
    }
  })
  
}