'use strict' 

const moment = require('moment')
const csv = require('csvtojson')

module.exports = async (fastify, opts) => { 
  fastify.put('/', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  }, async (request, reply) => {
    const { body } = request
    const { dateFromCsvToOracleFormat } = fastify.parser
    
    if (!body['booking_no']) return reply.code(400).send({ message: 'booking_no. is null' })

    const hasFrieght = await fastify.service(fastify.oracle).isHasFrieght(body['booking_no'])
    if (!hasFrieght) {
      return reply.code(400).send({ message: `Not found booking_no ${body['booking_no']}` })
    }

    await fastify.service(fastify.oracle).upsertContainer({
      NO: body['no'],
      booking_no: body['booking_no']
    },{
      EMPTY_CONTAINER_DEPOT_NO: body['empty_container_depot_no'],
      EMPTY_CONTAINER_DEPOT_DATE: dateFromCsvToOracleFormat(body['empty_container_depot_date']),
      '"SIZE"': body['size'],
      EMPTY_CONTAINER_OUT_TH_DATE: dateFromCsvToOracleFormat(body['empty_container_out_th_date']),
      EMPTY_CONTAINER_OUT_TH_TRUCK: body['empty_container_out_th_truck'],
      LADEN_COME_BACK_TO_YARD_DATE: dateFromCsvToOracleFormat(body['laden_come_back_to_yard_date']),
      LADEN_COME_BACK_TO_YARD_TRUCK: body['laden_come_back_to_yard_truck'],
      LADEN_CONTAINER_OUT_PORT_DATE: dateFromCsvToOracleFormat(body['laden_container_out_port_date']),
      LADEN_CONTAINER_OUT_PORT_TRUCK: body['laden_container_out_port_truck'],
      FORWARDING_RELEASE_PORT_DATE: dateFromCsvToOracleFormat(body['forwarding_release_port_date']),
      FORWARDING_RELEASE_PORT_TIME: body['forwarding_release_port_time'],
      SEAL_NO: body['seal_no'],
      DEPOT: body['depot'],
      FREE_TIME: body['free_time'],
      SHIPPING_AGENT: body['shipping_agent'],
      CLOSING_TIME: body['closing_time'],
      VESSEL: body['vessel'],
      VOL: body['vol'],
      VENDOR: body['vendor'],
      CLOSING_DATE: body['closing_date'],
      EMPTY_CONTAINER_DEPOT_TRUCK_NO: body['empty_container_depot_truck']
    })
    return { message: 'Upsert container data success!' }
  })
}

module.exports.autoPrefix = '/container'