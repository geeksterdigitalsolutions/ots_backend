'use strict' 

const moment = require('moment')

module.exports = async (fastify, opts) => { 
  fastify.get('/', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  },async (request) => {
    const { query } = request

    const response = await fastify.service(fastify.oracle).getAdminContainerList(query)

    response.items = response.items.map(item => {
      item.CLOSING_DATE = moment(item.CLOSING_DATE, "DD-MM-YY").format("DD/MM/YYYY")
      return item
    })
    
    return response
  })

  fastify.get('/export/all', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  },async (request) => {
    const { query } = request

    const response = await fastify.service(fastify.oracle).getAdminContainerListDetail(query)
    
    return response
  })
}