'use strict' 

const moment = require('moment')
const csv = require('csvtojson')

module.exports = async (fastify, opts) => { 
  fastify.post('/', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  }, async (request, reply) => {
    try {
      const { container_csv } = request.raw.files
      const { dateFromCsvToOracleFormat } = fastify.parser
      let containerCSVData = await csv({ noheader:true }).fromString(container_csv.data.toString())
      
      containerCSVData.shift()
      const errors = await fastify.validator.containerImportValidate(containerCSVData)
      if (errors.length > 0) {
        return reply.code(400).send({ errors })
      }

      for (let c of containerCSVData) {
        await fastify.service(fastify.oracle).upsertContainer({
          NO: c['field1'],
          booking_no: c['field2']
        },{
          EMPTY_CONTAINER_DEPOT_NO: c['field10'],
          EMPTY_CONTAINER_DEPOT_DATE: dateFromCsvToOracleFormat(c['field11']),
          '"SIZE"': c['field13'],
          EMPTY_CONTAINER_OUT_TH_DATE: dateFromCsvToOracleFormat(c['field14']),
          EMPTY_CONTAINER_OUT_TH_TRUCK: c['field15'],
          LADEN_COME_BACK_TO_YARD_DATE: dateFromCsvToOracleFormat(c['field16']),
          LADEN_COME_BACK_TO_YARD_TRUCK: c['field17'],
          LADEN_CONTAINER_OUT_PORT_DATE: dateFromCsvToOracleFormat(c['field18']),
          LADEN_CONTAINER_OUT_PORT_TRUCK: c['field19'],
          FORWARDING_RELEASE_PORT_DATE: dateFromCsvToOracleFormat(c['field20']),
          FORWARDING_RELEASE_PORT_TIME: c['field21'],
          SEAL_NO: c['field22'],
          DEPOT: c['field5'],
          FREE_TIME: c['field8'],
          SHIPPING_AGENT: c['field3'],
          CLOSING_TIME: c['field7'],
          VESSEL: c['field9'],
          VOL: c['field4'],
          VENDOR: c['field23'],
          CLOSING_DATE: c['field6'],
          EMPTY_CONTAINER_DEPOT_TRUCK_NO: c['field12']
        })
      }
      return { message: 'Import success!' }
    } catch (e) {
      return reply.code(500).send({ message: e.message })
    }
  })
}

module.exports.autoPrefix = '/container'