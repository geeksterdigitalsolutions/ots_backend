'use strict' 

const moment = require('moment')

module.exports = async (fastify, opts) => { 
  fastify.get('/dashboard', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  },async (request) => {
    const response = fastify.service(fastify.oracle).getAdminContainerDashboard()
    return response
  })
}
