'use strict' 

const moment = require('moment')

module.exports = async (fastify, opts) => { 
  fastify.get('/:booking_no', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  },async (request) => {
    const { params } = request
    
    let response = await fastify.service(fastify.oracle).getContainerDetail(params.booking_no)

    response = response.map(item => {
      item.CLOSING_DATE = moment(item.CLOSING_DATE, "DD/MM/YY")
      return item
    })

    return response
  })
}
