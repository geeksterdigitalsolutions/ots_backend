'use strict' 

const moment = require('moment')
const csv = require('csvtojson')

module.exports = async (fastify, opts) => { 
  fastify.delete('/:customer_id', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  }, async (request, reply) => {
    const { params } = request
    const response = await fastify.service(fastify.oracle).deleteCustomer({ id: params.customer_id})
    return { message: 'Customer has been removed' }
  })
}