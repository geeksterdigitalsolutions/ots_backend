'use strict' 

module.exports = async (fastify, opts) => { 
  fastify.post('/', {
    schema: {
      body: { 
        type: 'object',
        properties: {
          username: { type: 'string' },
          password: { type: 'string' },
          customer_code: { type: 'string' }
        }
      }
    },
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  }, async (request, reply) => {
    const { body } = request
    const customer = await fastify.service(fastify.oracle).findByUsername(body.username)
    if (customer.length > 0) {
      return reply.code(422).send({ message: 'Duplicate username' })
    }
    const response = await fastify.service(fastify.oracle).createCustomer(body)
    return { message: 'Customer has been created' }
  })
}