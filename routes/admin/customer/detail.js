'use strict' 

module.exports = async (fastify, opts) => { 
  fastify.get('/:customer_id', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  },async (request) => {
    const { params } = request
    
    return fastify.service(fastify.oracle).getCustomerDetail(params.customer_id)
  })
}