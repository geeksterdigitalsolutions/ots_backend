'use strict' 

module.exports = async (fastify, opts) => { 
  fastify.patch('/:customer_id', {
    schema: {
      body: { 
        type: 'object',
        properties: {
          username: { type: 'string' },
          password: { type: 'string' },
          customer_code: { type: 'string' }
        }
      }
    },
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  }, async (request, reply) => {
    const { body, params } = request
    const customer = await fastify.service(fastify.oracle).getCustomerDetail(params.customer_id)

    const data = {
      USERNAME: body.USERNAME,
      PASSWORD: body.PASSWORD ? await hash(body.password, 10) : customer[0].PASSWORD,
      CUSTOMER_CODE: body.CUSTOMER_CODE,
      CREATED_AT: customer[0].CREATED_AT,
      TYPE: body.TYPE
    }
    
    const response = await fastify.service(fastify.oracle).updateCustomer({ id: params.customer_id }, data)
    return { message: 'Customer has been updated' }
  })
}