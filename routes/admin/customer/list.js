'use strict' 
const moment = require('moment')
module.exports = async (fastify, opts) => { 
  fastify.get('/', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  },async (request) => {
    const { query } = request
    
    const response = await fastify.service(fastify.oracle).getCustomerList(query)
    
    response.items = response.items.map(item => ({ ...item, PASSWORD: null }))

    return response
  })
}