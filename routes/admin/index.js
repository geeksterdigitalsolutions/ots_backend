'use strict' 

const fs = require('fs')
const path = require('path')

module.exports = async (fastify, opts) => { 

  const filename = path.basename(__filename)
  const prefix = 'admin'
  const directories = fs.readdirSync(__dirname)
  let routeFiles = []
  for (let directory of directories) {
    if (directory !== filename) {
      routeFiles = fs.readdirSync(path.join(__dirname, directory))
      for (let routeFile of routeFiles) {
        fastify.register(require(path.join(__dirname, directory, routeFile)), { prefix: `${prefix}/${directory}` })
      }
    }
  }
}