'use strict' 

const moment = require('moment')

module.exports = async (fastify, opts) => { 
  fastify.get('/', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  },async (request) => {
    const { params, query } = request
    
    if (query['ETA_PORT_DESTINATION_FROM'] || query['ETA_PORT_DESTINATION_TO']) {
      query['ETA_PORT_DESTINATION'] = {
        from: query['ETA_PORT_DESTINATION_FROM'] || '',
        to: query['ETA_PORT_DESTINATION_TO'] || ''
      }
      delete query['ETA_PORT_DESTINATION_FROM']
      delete query['ETA_PORT_DESTINATION_TO']
    }

    if (query['ETD_PORT_OF_LOADING_FROM'] || query['ETD_PORT_OF_LOADING_TO']) {
      query['ETD_PORT_OF_LOADING'] = {
        from: query['ETD_PORT_OF_LOADING_FROM'] || '2019-09-01',
        to: query['ETD_PORT_OF_LOADING_TO'] || '2019-11-30',
      }
      delete query['ETD_PORT_OF_LOADING_FROM']
      delete query['ETD_PORT_OF_LOADING_TO']
    }

    if (query['SHIP_DATE_FROM'] || query['SHIP_DATE_TO']) {
      query['SHIP_DATE'] = {
        from: query['SHIP_DATE_FROM'] || '',
        to: query['SHIP_DATE_TO'] || '',
      }
      delete query['SHIP_DATE_FROM']
      delete query['SHIP_DATE_TO']
    }
    const response = await fastify.service(fastify.oracle).getAdminDocumentList(query) 
    const documentConfig = await fastify.service(fastify.oracle).getDocumentConfig()
    
    response.items = response.items.map(doc => ({
      default_link: documentConfig.default_link,
      ...doc,
      STATUS_OF_DOCUMENT: doc.STATUS_OF_DOCUMENT === 'completed' ? 'completed' : 'pending',
      CBR_PLAN: moment(doc['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').subtract(2, 'days').format('DD-MMM-YY'),
      SI_PLAN: moment(doc['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').subtract(2, 'days').format('DD-MMM-YY'),
      DRAFT_BL_PLAN: moment(doc['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').subtract(1, 'days').format('DD-MMM-YY'),
      CONFIRM_BL_PLAN: doc['ETD_PORT_OF_LOADING'],
      OBL_PLAN: moment(doc['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').add(1, 'days').format('DD-MMM-YY'),
      APPLY_PSYTO_PLAN: doc['ETD_PORT_OF_LOADING'],
      RECEIVE_PSYTO_PLAN: doc['ETD_PORT_OF_LOADING'],
      APPLY_COCI_CHAMBER_PLAN: doc['ETD_PORT_OF_LOADING'],
      RECEIVE_COCI_CHAMBER_PLAN: moment(doc['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').add(2, 'days').format('DD-MMM-YY'),
      APPLY_FTA_FORM_PLAN: doc['ETD_PORT_OF_LOADING'],
      RECEIVE_FTA_FORM_PLAN: moment(doc['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').add(2, 'days').format('DD-MMM-YY'),
      BE_PLAN: doc['ETD_PORT_OF_LOADING'],
      SCAN_SENT_EMAIL_PLAN: doc['ETD_PORT_OF_LOADING'],
      COMPLETE_SET_PLAN: moment(doc['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').add(3, 'days').format('DD-MMM-YY')
    }))
    return response
  })
}

module.exports.autoPrefix = '/document'