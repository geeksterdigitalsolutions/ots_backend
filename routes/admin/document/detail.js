'use strict' 

const moment = require('moment')

module.exports = async (fastify, opts) => { 
  fastify.get('/:booking_no/:delivery_no', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  },async (request) => {
    const { params } = request
    const response = await fastify.service(fastify.oracle).getDocumentDetail(params.booking_no, params.delivery_no)
    const documentConfig = await fastify.service(fastify.oracle).getDocumentConfig()

    return response.map(doc => ({
      default_link: documentConfig.default_link,
      ...doc,
      STATUS_OF_DOCUMENT: doc.STATUS_OF_DOCUMENT === 'completed' ? 'completed' : 'pending',
      CBR_PLAN: moment(doc['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').subtract(2, 'days').format('DD-MMM-YY'),
      SI_PLAN: moment(doc['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').subtract(2, 'days').format('DD-MMM-YY'),
      DRAFT_BL_PLAN: moment(doc['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').subtract(1, 'days').format('DD-MMM-YY'),
      CONFIRM_BL_PLAN: moment(doc['ETD_PORT_OF_LOADING']).format('DD-MMM-YY'),
      OBL_PLAN: moment(doc['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').add(1, 'days').format('DD-MMM-YY'),
      APPLY_PSYTO_PLAN: moment(doc['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').format('DD-MMM-YY'),
      RECEIVE_PSYTO_PLAN: moment(doc['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').format('DD-MMM-YY'),
      APPLY_COCI_CHAMBER_PLAN: moment(doc['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').format('DD-MMM-YY'),
      RECEIVE_COCI_CHAMBER_PLAN: moment(doc['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').add(2, 'days').format('DD-MMM-YY'),
      APPLY_FTA_FORM_PLAN: moment(doc['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').format('DD-MMM-YY'),
      RECEIVE_FTA_FORM_PLAN: moment(doc['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').add(2, 'days').format('DD-MMM-YY'),
      BE_PLAN: moment(doc['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').format('DD-MMM-YY'),
      SCAN_SENT_EMAIL_PLAN: moment(doc['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').format('DD-MMM-YY'),
      COMPLETE_SET_PLAN: moment(doc['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').add(3, 'days').format('DD-MMM-YY')
    }))
  })
}

module.exports.autoPrefix = '/document'