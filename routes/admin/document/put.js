'use strict' 

const moment = require('moment')
const csv = require('csvtojson')

module.exports = async (fastify, opts) => { 
  fastify.put('/', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  }, async (request, reply) => {
    const { body } = request
    const { dateFromCsvToOracleFormat } = fastify.parser
    if (!body['do']) throw { message: 'Delivery No. is null' }
    // if (!body['actual_eta']) return reply.code(400).send({ message: 'Actual ETA is null' })
    if (!body['etd_port_of_loading']) return reply.code(400).send({ message: 'ETD Port of loading is null' })
    if (!body['so']) return reply.code(400).send({ message: 'SO No. is null' })
    if (!body['booking_no']) return reply.code(400).send({ message: 'Booking No. is null' })

    let status = 'pending'
    const shipmentStatusComplete = await fastify.service(fastify.oracle).checkShipmentStatusComplete(body['booking_no'], body['do'])
    if (shipmentStatusComplete && body['status_of_document'] == 'completed') {
      status = 'completed'
    }

    await fastify.service(fastify.oracle).upsertMasterOrder({
      delivery_no:    body['do'],
      ci_no:          body['commercial_invoice'],
      so_no:          body['so'],
      booking_no:     body['booking_no']
    },{
      status // FIXME: Status จะ completed ต่อเมื่อ import ทั้ง frieght และ document แล้ว
    })

    await fastify.service(fastify.oracle).upsertDocument({
      booking_no: body['booking_no'],
      delivery_no: body['do']
    },{
      form: body['form'],
      responsibility: body['responsibility'],
      lc_no: body['lc_no'],
      receive_lc: dateFromCsvToOracleFormat(body['receive_lc']),
      lc_expiry: dateFromCsvToOracleFormat(body['lc_expiry']),
      lds: dateFromCsvToOracleFormat(body['lds']),
      period_of_presentation: body['period_of_presention'],
      cbr_actual: dateFromCsvToOracleFormat(body['cbr_actual']),
      si_actual: dateFromCsvToOracleFormat(body['si_actual']),
      draft_bl_actual: dateFromCsvToOracleFormat(body['draft_bl_actual']),
      confirm_bl_actual: dateFromCsvToOracleFormat(body['confirm_bl_actual']),
      obl_actual: dateFromCsvToOracleFormat(body['obl_actual']),
      apply_psyto_actual: dateFromCsvToOracleFormat(body['apply_psyto_actual']),
      receive_psyto_actual: dateFromCsvToOracleFormat(body['receive_psyto_actual']),
      apply_coci_chamber_actual: dateFromCsvToOracleFormat(body['apply_coci_chamber_actual']),
      receive_coci_chamber_actual: dateFromCsvToOracleFormat(body['receive_coci_chamber_actual']),
      apply_fta_form_actual: dateFromCsvToOracleFormat(body['apply_fta_form_actual']),
      receive_fta_form_actual: dateFromCsvToOracleFormat(body['receive_fta_form_actual']),
      be_actual: dateFromCsvToOracleFormat(body['be_actual']),
      scan_sent_email_actual: dateFromCsvToOracleFormat(body['scan_sent_email_actual']),
      complete_set_actual: dateFromCsvToOracleFormat(body['complete_set_actual']),
      status_remark: body['status_remark'],
      courier_date: dateFromCsvToOracleFormat(body['courier_date']),
      courier_by: body['courier_by'],
      tracking_no: body['tracking_no'],
      status_of_document: body['status_of_document'] ? body['status_of_document'] : 'pending',
      total_days_spent: body['total_days_spent'],
      bank_ref_no: body['bank_ref_no'],
      sst_actual: dateFromCsvToOracleFormat(body['sst_actual']),
      permit_actual: dateFromCsvToOracleFormat(body['permit_actual']),
      no_of_pending_day: body['no_of_pending_day'] ? body['no_of_pending_day'] : null,
      sales_person: body['sales_person'],
      bl_no: body['bl_no'],
      display_cbr: body['display_cbr'],
      display_si: body['display_si'],
      display_draft_bl: body['display_draft_bl'],
      display_confirm_bl: body['display_confirm_bl'],
      display_obl_bl: body['display_obl_bl'],
      display_apply_psyto: body['display_apply_psyto'],
      display_receive_psyto: body['display_receive_psyto'],
      display_apply_coci_chamber: body['display_apply_coci_chamber'],
      display_receive_coci_chamber: body['display_receive_coci_chamber'],
      display_apply_fta_form_plan: body['display_apply_fta_form_plan'],
      display_receive_fta_form_plan: body['display_receive_fta_form_plan'],
      display_be_plan: body['display_be_plan'],
      display_scan_sent_email_plan: body['display_scan_sent_email_plan'],
      display_complete_set_plan: body['display_complete_set_plan'],
    })
    
    return { message: 'Upsert document data success!' }
  })
}

module.exports.autoPrefix = '/document'