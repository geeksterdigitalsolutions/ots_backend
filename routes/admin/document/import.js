'use strict' 

const moment = require('moment')
const csv = require('csvtojson')

module.exports = async (fastify, opts) => { 
  fastify.post('/', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  }, async (request, reply) => {
    try {
      const { document_csv } = request.raw.files
      const { dateFromCsvToOracleFormat } = fastify.parser
      let documentCSVData = await csv().fromString(document_csv.data.toString())
      
      const errors = await fastify.validator.documentImportValidate(documentCSVData)
      if (errors.length > 0) {
        return reply.code(400).send({ errors })
      }

      for (let d of documentCSVData) {
        let status = 'pending'
        const shipmentStatusComplete = await fastify.service(fastify.oracle).checkShipmentStatusComplete(d['booking_no'], d['do'])
        if (shipmentStatusComplete && d['status_of_document'] == 'completed') {
          status = 'completed'
        }

        await fastify.service(fastify.oracle).upsertMasterOrder({
          delivery_no:    d['do'],
          ci_no:          d['commercial_invoice'],
          so_no:          d['so'],
          booking_no:     d['booking_no']
        },{
          status // FIXME: Status จะ completed ต่อเมื่อ import ทั้ง frieght และ document แล้ว
        })

        await fastify.service(fastify.oracle).upsertDocument({
          booking_no: d['booking_no'],
          delivery_no: d['do']
        },{
          form: d['form'],
          responsibility: d['responsibility'],
          lc_no: d['lc_no'],
          receive_lc: dateFromCsvToOracleFormat(d['receive_lc']),
          lc_expiry: dateFromCsvToOracleFormat(d['lc_expiry']),
          lds: dateFromCsvToOracleFormat(d['lds']),
          period_of_presentation: d['period_of_presention'],
          cbr_actual: dateFromCsvToOracleFormat(d['cbr_actual']),
          si_actual: dateFromCsvToOracleFormat(d['si_actual']),
          draft_bl_actual: dateFromCsvToOracleFormat(d['draft_bl_actual']),
          confirm_bl_actual: dateFromCsvToOracleFormat(d['confirm_bl_actual']),
          obl_actual: dateFromCsvToOracleFormat(d['obl_actual']),
          apply_psyto_actual: dateFromCsvToOracleFormat(d['apply_psyto_actual']),
          receive_psyto_actual: dateFromCsvToOracleFormat(d['receive_psyto_actual']),
          apply_coci_chamber_actual: dateFromCsvToOracleFormat(d['apply_coci_chamber_actual']),
          receive_coci_chamber_actual: dateFromCsvToOracleFormat(d['receive_coci_chamber_actual']),
          apply_fta_form_actual: dateFromCsvToOracleFormat(d['apply_fta_form_actual']),
          receive_fta_form_actual: dateFromCsvToOracleFormat(d['receive_fta_form_actual']),
          be_actual: dateFromCsvToOracleFormat(d['be_actual']),
          scan_sent_email_actual: dateFromCsvToOracleFormat(d['scan_sent_email_actual']),
          complete_set_actual: dateFromCsvToOracleFormat(d['complete_set_actual']),
          status_remark: d['status_remark'],
          courier_date: dateFromCsvToOracleFormat(d['courier_date']),
          courier_by: d['courier_by'],
          tracking_no: d['tracking_no'],
          status_of_document: d['status_of_document'] ? d['status_of_document'] : 'pending',
          total_days_spent: d['total_days_spent'],
          bank_ref_no: d['bank_ref_no'],
          sst_actual: dateFromCsvToOracleFormat(d['sst_actual']),
          permit_actual: dateFromCsvToOracleFormat(d['permit_actual']),
          no_of_pending_day: d['no_of_pending_day'] ? d['no_of_pending_day'] : null,
          sales_person: d['sales_person'],
          bl_no: d['bl_no'],
          DISPLAY_CBR: 0,
          DISPLAY_SI: 0,
          DISPLAY_DRAFT_BL: 0,
          DISPLAY_CONFIRM_BL: 0,
          DISPLAY_OBL_BL: dateFromCsvToOracleFormat(d['obl_actual']) ? 1 : 0,
          DISPLAY_APPLY_PSYTO: 0,
          DISPLAY_RECEIVE_PSYTO: dateFromCsvToOracleFormat(d['receive_psyto_actual']) ? 1 : 0,
          DISPLAY_APPLY_COCI_CHAMBER: 0,
          DISPLAY_RECEIVE_COCI_CHAMBER: dateFromCsvToOracleFormat(d['receive_coci_chamber_actual']) ? 1 : 0,
          DISPLAY_APPLY_FTA_FORM_PLAN: 0,
          DISPLAY_RECEIVE_FTA_FORM_PLAN: dateFromCsvToOracleFormat(d['receive_fta_form_actual']) ? 1 : 0,
          DISPLAY_BE_PLAN: 0,
          DISPLAY_SCAN_SENT_EMAIL_PLAN: dateFromCsvToOracleFormat(d['scan_sent_email_actual']) ? 1 : 0,
          DISPLAY_COMPLETE_SET_PLAN: dateFromCsvToOracleFormat(d['complete_set_actual']) ? 1 : 0
        })
      }
      
      return { message: 'Import success!' }
    } catch (e) {
      return reply.code(500).send({ message: e.message })
    }
    
  })
}

module.exports.autoPrefix = '/document'