'use strict' 

const moment = require('moment')
const csv = require('csvtojson')

const checkWaitingPass = (val) => {
  return ['Not Ready', 'Waiting C3'].includes(val) === false
}

module.exports = async (fastify, opts) => { 
  fastify.post('/', {
    preValidation: [
      // fastify.oauth.verifyAccessToken
    ]
  }, async (request, reply) => {
    try {
      const { document_csv } = request.raw.files
      const { dateFromCsvToOracleFormat } = fastify.parser
      let documentCSVData = await csv({ noheader:true }).fromString(document_csv.data.toString())
      // return documentCSVData[0]

      let waiting_sst = null
      let waiting_permit = null
      let waiting_phyto = null
      let waiting_form_d = null
      let status = ''

      documentCSVData.shift()
      const errors = await fastify.validator.documentHyImportValidate(documentCSVData)
      if (errors.length > 0) {
        return reply.code(400).send({ errors })
      }

      for (let d of documentCSVData) {
        
        waiting_sst = d['field76']
        waiting_permit = d['field77']
        waiting_phyto = d['field78']
        waiting_form_d = d['field79']

        if (waiting_sst == '' && waiting_permit == '' && waiting_phyto == '' && waiting_form_d == '') {
          status = ''
        } else {
          status = checkWaitingPass(waiting_sst) && checkWaitingPass(waiting_permit) && checkWaitingPass(waiting_phyto) && checkWaitingPass(waiting_form_d) ? 'completed' : 'pending'
        }
        
        await fastify.service(fastify.oracle).upsertDocumentHY({
          so: d['field1'],
          item: d['field4'],
          do: d['field3'],
        },{
          commercial_invoice: d['field5'],
          customer_name: d['field10'],
          destination_port: d['field37'],
          payment_term: d['field41'],
          plan_gl_ship_date: dateFromCsvToOracleFormat(d['field6']),
          etd: dateFromCsvToOracleFormat(d['field49']),
          eta: dateFromCsvToOracleFormat(d['field50']),
          waiting_sst,
          waiting_permit,
          waiting_phyto,
          waiting_form_d,
          status
        })
      }
      
      return { message: 'Import success!' }
    } catch (e) {
      return reply.code(500).send({ message: e.message })
    }
  })
}

module.exports.autoPrefix = '/document-hy'