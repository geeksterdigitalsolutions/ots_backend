'use strict' 

const moment = require('moment')
const csv = require('csvtojson')

module.exports = async (fastify, opts) => { 
  fastify.delete('/:delivery_no/:so', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  }, async (request, reply) => {
    const { delivery_no, so } = request.params
    await fastify.service(fastify.oracle).removeDocumentHy({ do: delivery_no, so })
    return { message: `Has been remove document hy as do: ${delivery_no}, so: ${so} ` }
  })

  fastify.delete('/:delivery_no/:so/:item', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  }, async (request, reply) => {
    const { delivery_no, so, item } = request.params
    await fastify.service(fastify.oracle).removeDocumentHy({ do: delivery_no, item, so })
    return { message: `Has been remove document hy as do: ${delivery_no}, so: ${so}, item: ${item} ` }
  })
}

module.exports.autoPrefix = '/document-hy'