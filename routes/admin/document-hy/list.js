'use strict' 

const moment = require('moment')

module.exports = async (fastify, opts) => { 
  fastify.get('/', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  },async (request) => {
    const { params, query } = request

    if (query['ETD_FROM'] || query['ETD_TO']) {
      query['ETD'] = {
        from: query['ETD_FROM'] || '2019-09-01',
        to: query['ETD_TO'] || '2019-11-30',
      }
      delete query['ETD_FROM']
      delete query['ETD_TO']
    }
    if (query['ETA_FROM'] || query['ETA_TO']) {
      query['ETA'] = {
        from: query['ETA_FROM'] || '2019-09-01',
        to: query['ETA_TO'] || '2019-11-30',
      }
      delete query['ETA_FROM']
      delete query['ETA_TO']
    }
    const response = await fastify.service(fastify.oracle).getAdminDocumentListHY(query) 
    return response
  })
}

module.exports.autoPrefix = '/document-hy'