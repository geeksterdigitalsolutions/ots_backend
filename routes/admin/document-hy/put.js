'use strict' 

const moment = require('moment')
const csv = require('csvtojson')

const checkWaitingPass = (val) => {
  return ['Not Ready', 'Waiting C3'].includes(val) === false
}

module.exports = async (fastify, opts) => { 
  fastify.put('/', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  }, async (request, reply) => {
    const { body } = request
    const { dateFromCsvToOracleFormat } = fastify.parser
    

    if (!body['so']) throw { message: 'SO No. is null' }
    if (!body['item']) throw { message: 'Item is null' }

    const waiting_sst = body['waiting_sst']
    const waiting_permit = body['waiting_permit']
    const waiting_phyto = body['waiting_phyto']
    const waiting_form_d = body['waiting_form_d']
    let status = ''
    
    if (waiting_sst != '' && waiting_permit != '' && waiting_phyto != '' && waiting_form_d != '') {
      status = checkWaitingPass(waiting_sst) && checkWaitingPass(waiting_permit) && checkWaitingPass(waiting_phyto) && checkWaitingPass(waiting_form_d) ? 'completed' : 'pending'
    }

    await fastify.service(fastify.oracle).upsertDocumentHY({
      so: body['so'],
      item: body['item'],
      do: body['do'],
    },{
      commercial_invoice: body['commercial_invoice'],
      customer_name: body['customer_name'],
      destination_port: body['destination_port'],
      payment_term: body['payment_term'],
      plan_gl_ship_date: dateFromCsvToOracleFormat(body['plan_gl_ship_date']),
      etd: dateFromCsvToOracleFormat(body['etd']),
      eta: dateFromCsvToOracleFormat(body['eta']),
      waiting_sst,
      waiting_permit,
      waiting_phyto,
      waiting_form_d,
      status
    })
    
    return { message: 'Upsert document HY data success!' }
  })
}

module.exports.autoPrefix = '/document-hy'