'use strict' 

const moment = require('moment')

module.exports = async (fastify, opts) => { 
  fastify.get('/:so/:do/:item', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  },async (request) => {
    const { params } = request
    const response = await fastify.service(fastify.oracle).getDocumentDetailHY(params.so, params.do, params.item)
    return response
  })
}

module.exports.autoPrefix = '/document-hy'