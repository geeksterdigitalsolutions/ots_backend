'use strict' 

const moment = require('moment')

module.exports = async (fastify, opts) => { 
  fastify.get('/', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  },async (request) => {
    const { params } = request
    const response = await fastify.service(fastify.oracle).getDocumentConfig()

    return response
  })
}