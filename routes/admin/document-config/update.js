'use strict' 

const moment = require('moment')
const csv = require('csvtojson')

module.exports = async (fastify, opts) => { 
  fastify.patch('/', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  }, async (request, reply) => {
    const { body } = request
    const name = Object.keys(body)[0]
    const value = body[name]
    await fastify.service(fastify.oracle).updateDocumentConfig({name}, {value})
    return { message: 'Update document config' }
  })
}

module.exports.autoPrefix = '/document-hy'