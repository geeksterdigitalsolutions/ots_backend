'use strict' 

const moment = require('moment')
const csv = require('csvtojson')

module.exports = async (fastify, opts) => { 
  fastify.post('/', {
    preValidation: [
      await fastify.oauth.verifyAccessToken
    ]
  }, async (request, reply) => {
    try {
      const { truck_csv } = request.raw.files
      // const { dateFromCsvToOracleFormat } = fastify.parser
      let truckCSVData = await csv({ noheader:true }).fromString(truck_csv.data.toString())
      
      truckCSVData.shift()

      const errors = await fastify.validator.truckImportValidate(truckCSVData)
      if (errors.length > 0) {
        return reply.code(400).send({ errors })
      }
      
      for (let t of truckCSVData) {
        await fastify.service(fastify.oracle).upsertTruck({
          do: t['field10'],
          invoice_no: t['field9']
        }, {
          cargo_border_to_bkhwh_date: t['field18'],
          cargo_border_to_bkhwh_time: t['field19'],
          my_driver: t['field20'],
          my_truck_no: t['field21'],
          d_o_no: t['field22'],
          cargo_bkhwh_to_customer_date: t['field23'],
          cargo_bkhwh_to_customer_time: t['field24'],
          consignee_name: t['field25'],
          location: t['field26'],
          estimate_arrival_date: t['field27'],
          estimate_arrival_time: t['field28'],
          cargo_pnp_to_cross_border_date: t['field7'],
          cargo_pnp_to_cross_border_time: t['field8'],
          shipping_my: t['field15'],
          transportation_on_name: t['field16'],
          warehouse_name: t['field17']
        })
      }
      
      return { message: 'Import success!' }
    } catch (e) {
      return reply.code(500).send({ message: e.message })
    }
  })
}

module.exports.autoPrefix = '/truck'