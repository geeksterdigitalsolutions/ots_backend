'use strict' 

const axios = require('axios')
const moment = require('moment')

module.exports = async (fastify, opts) => { 
  fastify.get('/:so_no/:ci_no/:pi_no/:po_no/:customer_code/:so_item', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  }, async (request) => {
    const { params } = request
    const response = await fastify.service(fastify.oracle).getTruckDetail(params.so_no, params.ci_no, params.pi_no, params.po_no, params.customer_code, params.so_item)
    let items = []
    for (let t of response) {
      const { PI_NO: delivery, SO_ITEM: item } = t
      const responseTms = await axios.get('http://dev.panelplus.co.th/logistics_v2/model/get_vehicle_detail.php', { params: {delivery, item} })
      t.THAI_DRIVER = responseTms.data ? responseTms.data[0].driver : null
      t.THAI_TRUCK_NO = responseTms.data ? responseTms.data[0].car_id : null
      t.CARGO_DEPARTURE_FROM_PNP_DATE_IN = responseTms.data && responseTms.data[0].atd ? moment(responseTms.data[0].atd.date) : null
      t.CARGO_DEPARTURE_FROM_PNP_TIME_OUT = responseTms.data && responseTms.data[0].actual_time_out ? moment(responseTms.data[0].actual_time_out.date).format("HH:mm") : null
      t.CARGO_ARRIVAL_TO_PNP_TIME_IN =responseTms.data && responseTms.data[0].actual_time_in ? moment(responseTms.data[0].actual_time_in.date).format("HH:mm") : null
      t.CARGO_BORDER_TO_BKHWH_DATE = t.CARGO_BORDER_TO_BKHWH_DATE ? moment(t.CARGO_BORDER_TO_BKHWH_DATE, 'DD/MM/YYYY') : null
      t.CARGO_BKHWH_TO_CUSTOMER_DATE = t.CARGO_BKHWH_TO_CUSTOMER_DATE ? moment(t.CARGO_BKHWH_TO_CUSTOMER_DATE, 'DD/MM/YYYY') : null
      t.ESTIMATE_ARRIVAL_DATE = t.ESTIMATE_ARRIVAL_DATE ? moment(t.ESTIMATE_ARRIVAL_DATE, 'DD/MM/YYYY') : null
      t.CARGO_PNP_TO_CROSS_BORDER_DATE = t.CARGO_PNP_TO_CROSS_BORDER_DATE ? moment(t.CARGO_PNP_TO_CROSS_BORDER_DATE, 'DD/MM/YYYY') : null
      items.push(t)
    }
    return items
  })
}

module.exports.autoPrefix = '/truck'