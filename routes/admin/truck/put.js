'use strict' 

const moment = require('moment')
const csv = require('csvtojson')

module.exports = async (fastify, opts) => { 
  fastify.put('/', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  }, async (request, reply) => {
    const { body } = request
    
    // const truckCount = await fastify.service.countOfTruck()
    // let no = truckCount+1
    body['cargo_border_to_bkhwh_date'] = moment(body['cargo_border_to_bkhwh_date']).isValid() ? moment(body['cargo_border_to_bkhwh_date']).format('DD/MM/YYYY') : body['cargo_border_to_bkhwh_date']
    body['cargo_bkhwh_to_customer_date'] = moment(body['cargo_bkhwh_to_customer_date']).isValid() ? moment(body['cargo_bkhwh_to_customer_date']).format('DD/MM/YYYY') : body['cargo_bkhwh_to_customer_date']
    body['estimate_arrival_date'] = moment(body['estimate_arrival_date']).isValid() ? moment(body['estimate_arrival_date']).format('DD/MM/YYYY') : body['estimate_arrival_date']
    body['cargo_pnp_to_cross_border_date'] = moment(body['cargo_pnp_to_cross_border_date']).isValid() ? moment(body['cargo_pnp_to_cross_border_date']).format('DD/MM/YYYY') : body['cargo_pnp_to_cross_border_date']

    await fastify.service(fastify.oracle).upsertTruck({
      do: body['do'],
      invoice_no: body['invoice_no']
    },{
      cargo_border_to_bkhwh_date:  body['cargo_border_to_bkhwh_date'],
      cargo_border_to_bkhwh_time: body['cargo_border_to_bkhwh_time'],
      my_driver: body['my_driver'],
      my_truck_no: body['my_truck_no'],
      d_o_no: body['d_o_no'],
      cargo_bkhwh_to_customer_date: body['cargo_bkhwh_to_customer_date'],
      cargo_bkhwh_to_customer_time: body['cargo_bkhwh_to_customer_time'],
      consignee_name: body['consignee_name'],
      location: body['location'],
      estimate_arrival_date: body['estimate_arrival_date'],
      estimate_arrival_time: body['estimate_arrival_time'],
      cargo_pnp_to_cross_border_date: body['cargo_pnp_to_cross_border_date'],
      cargo_pnp_to_cross_border_time: body['cargo_pnp_to_cross_border_time'],
      shipping_my: body['shipping_my'],
      transportation_on_name: body['transportation_on_name'],
      warehouse_name: body['warehouse_name']
    })

    return { message: 'Upsert frieght data success!' }
  })
}

module.exports.autoPrefix = '/truck'