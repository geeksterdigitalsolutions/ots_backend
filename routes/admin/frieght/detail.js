'use strict' 

const moment = require('moment')

module.exports = async (fastify, opts) => { 
  fastify.get('/:booking_no/:delivery_no', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  },async (request) => {
    const { params, query } = request
    
    const response = await fastify.service(fastify.oracle).getFrightDetail(params.booking_no, params.delivery_no)

    if (response.length > 0) {
      response[0].MONTH = moment(response[0].MONTH, 'DD/MM/YYYY')

      if (!response[0].SHIPMENT_STATUS) {
        response[0].SHIPMENT_STATUS = 'pending'
      } 
      if (!response[0].DOCUMENT_STATUS) {
        response[0].DOCUMENT_STATUS = 'pending'
      }
      if (response[0].SHIPMENT_STATUS == 'completed' && response[0].DOCUMENT_STATUS == 'completed') {
        response[0].STATUS = 'completed'
      }
    }

    return response
  })
}

module.exports.autoPrefix = '/frieght'