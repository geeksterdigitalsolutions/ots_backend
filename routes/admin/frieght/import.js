'use strict' 

const moment = require('moment')
const csv = require('csvtojson')

module.exports = async (fastify, opts) => { 
  fastify.post('/', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  }, async (request, reply) => {
    try {
      const { frieght_csv } = request.raw.files
      const { dateFromCsvToOracleFormat } = fastify.parser
      let frieghtCSVData = await csv().fromString(frieght_csv.data.toString())

      const errors = await fastify.validator.frieghtImportValidate(frieghtCSVData)
      if (errors.length > 0) {
        return reply.code(400).send({ errors })
      }

      for (let f of frieghtCSVData) {
        const frieghtViewResponse = await fastify.service(fastify.oracle).findViewFrieghtByBookingNo(f['booking_no'])
        const frieghtView = frieghtViewResponse[0]

        let status = 'pending'
        const shipment_status = f['actual_eta'] ? 'completed' : 'pending'
        const document = await fastify.service(fastify.oracle).getDocumentDetail(f['booking_no'], f['delivery_no'])
        if (document.length > 0 && document[0]['STATUS_OF_DOCUMENT'] == 'completed' && shipment_status == 'completed') {
          status = 'completed'
        }
  
        await fastify.service(fastify.oracle).upsertMasterOrder({
          delivery_no:    f['delivery_no'],
          ci_no:          f['ci_no'],
          so_no:          f['so_no'],
          booking_no:     f['booking_no']
        },{
          status // FIXME: Status จะ completed ต่อเมื่อ import ทั้ง frieght และ document แล้ว
        })
        
        let eta_port_of_destination = moment(frieghtView.ETA, 'DD-MMM-YY')
  
        let actual_eta = null
        let on_time = null
  
        if (f['actual_eta'] && f['actual_eta'] !== '') {
          const actual_eta_moment = moment(f['actual_eta'], 'DD/MM/YYYY')
          actual_eta = actual_eta_moment.toDate()
          on_time = actual_eta_moment.diff(eta_port_of_destination, 'days')
        }
        
        await fastify.service(fastify.oracle).upsertFrieght({
          booking_no: f['booking_no'],
          delivery_no: f['delivery_no']
        },{
          border: f['border'],
          month: f['month'],
          liner: f['line_fwd'],
          fwd: f['line_fwd'],
          etd_port_of_loading: f['etd_port_of_loading'] ? moment(f['etd_port_of_loading'], 'DD/MM/YYYY').toDate() : null,
          actual_etd_port_of_loading: f['actutal_etd_port_of_loading'] ? moment(f['actutal_etd_port_of_loading'], 'DD/MM/YYYY').toDate() : null,
          feeder_name: f['feeder_name'],
          port_of_transhipment: f['port_of_transhipment'],
          eta_port_of_transhipment: f['eta_port_of_transshipment'] ? moment(f['eta_port_of_transshipment'], 'DD/MM/YYYY').toDate() : null,
          etd_port_of_transhipment: f['etd_transshipment_port'] ? moment(f['etd_transshipment_port'], 'DD/MM/YYYY').toDate() : null,
          mother_vessel_name: f['mother_vessel_name'],
          on_time, 
          delivery_on_time: fastify.parser.getDeliveryOnTimeFrieght(on_time),
          frieght_usd: f['usd'],
          VGM: f['vgm'],
          invoice: f['invoice'],
          '"DATE"': f['date'] ? moment(f['date'], 'DD/MM/YYYY').toDate() : null,
          due: f['due'] ? moment(f['due'], 'DD/MM/YYYY').toDate() : null,
          FRIEGHT_URL: f['url'],
          shipment_status,
          actual_etd: f['actual_etd'] ? moment(f['actual_etd'], 'DD/MM/YYYY').toDate() : null,
          responsibility: f['responsibility'],
          actual_eta,
          no_postpone: f['no_postpone'],
          remark: f['remark'],
          reason: f['reason'],
          baht: (typeof f['baht'] == "string") ? f['baht'].replace(',', '') : f['baht'],
          myr: (typeof f['myr'] == "string") ? f['myr'].replace(',', '') : f['myr'],
          place_bkg_date: dateFromCsvToOracleFormat(f['place_bkg_date']),
          release_bkg_date: dateFromCsvToOracleFormat(f['release_bkg_date']),
          reason_booking: f['reason_booking'],
          freetime: f['freetime'],
          reference: f['reference']
        })
      }
      return { message: 'Import success!' }
    } catch (e) {
      return reply.code(500).send({ message: e.message })
    }
  })
}

module.exports.autoPrefix = '/frieght'