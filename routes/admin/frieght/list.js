'use strict' 
const moment = require('moment')
module.exports = async (fastify, opts) => { 
  fastify.get('/', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  },async (request) => {
    const { query } = request
    
    if (query['ETD_PORT_OF_TRANSHIPMENT_FROM'] || query['ETD_PORT_OF_TRANSHIPMENT_TO']) {
      query['ETD_PORT_OF_TRANSHIPMENT'] = {
        from: query['ETD_PORT_OF_TRANSHIPMENT_FROM'] || '',
        to: query['ETD_PORT_OF_TRANSHIPMENT_TO'] || '',
      }
      delete query['ETD_PORT_OF_TRANSHIPMENT_FROM']
      delete query['ETD_PORT_OF_TRANSHIPMENT_TO']
    }
    if (query['ETA_PORT_OF_DESTINATION_FROM'] || query['ETA_PORT_OF_DESTINATION_TO']) {
      query['ETA_PORT_OF_DESTINATION'] = {
        from: query['ETA_PORT_OF_DESTINATION_FROM'] || '',
        to: query['ETA_PORT_OF_DESTINATION_TO'] || '',
      }
      delete query['ETA_PORT_OF_DESTINATION_FROM']
      delete query['ETA_PORT_OF_DESTINATION_TO']
    }
    if (query['ETD_PORT_FROM'] || query['ETD_PORT_TO']) {
      query['ETD_PORT'] = {
        from: query['ETD_PORT_FROM'] || moment().subtract(1, 'M').startOf('month').format('YYYY-MM-DD'),
        to: query['ETD_PORT_TO'] || moment().add(1, 'M').endOf('month').format('YYYY-MM-DD'),
      }
      delete query['ETD_PORT_FROM']
      delete query['ETD_PORT_TO']
    }
    
    const resultData = await fastify.service(fastify.oracle).getFrieghtAdminList(query)
    resultData.items = resultData.items.map(f => {
      if (!f.SHIPMENT_STATUS) {
        f.SHIPMENT_STATUS = 'pending'
      } 
      if (!f.DOCUMENT_STATUS) {
        f.DOCUMENT_STATUS = 'pending'
      }
      if (f.SHIPMENT_STATUS == 'completed' && f.DOCUMENT_STATUS == 'completed') {
        f.STATUS = 'completed'
      }
      return f
    })
    return resultData
  })
}