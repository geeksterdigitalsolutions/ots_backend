'use strict' 

const moment = require('moment')
const csv = require('csvtojson')

module.exports = async (fastify, opts) => { 
  fastify.put('/', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  }, async (request, reply) => {
    const { body } = request
    const { dateFromCsvToOracleFormat } = fastify.parser

    if (!body['delivery_no']) return reply.code(400).send({ message: 'Delivery No. is null' })
    // if (!body['actual_eta']) return reply.code(400).send({ message: 'Actual ETA is null' })
    if (!body['so_no']) return reply.code(400).send({ message: 'SO No. is null' })
    if (!body['booking_no']) return reply.code(400).send({ message: 'Booking No. is null' })

    const frieghtViewResponse = await fastify.service(fastify.oracle).findViewFrieghtByBookingNo(body['booking_no'])
    if (frieghtViewResponse.length == 0) {
      return reply.code(400).send({ message: 'Not found ETA port of destination' })
    }

    const frieghtView = frieghtViewResponse[0]

    let status = 'pending'
    const shipment_status = body['actual_eta'] ? 'completed' : 'pending'
    const document = await fastify.service(fastify.oracle).getDocumentDetail(body['booking_no'], body['delivery_no'])

    if (document.length > 0 && document[0]['STATUS_OF_DOCUMENT'] == 'completed' && shipment_status == 'completed') {
      status = 'completed'
    }

    await fastify.service(fastify.oracle).upsertMasterOrder({
      delivery_no:    body['delivery_no'],
      ci_no:          body['ci_no'],
      so_no:          body['so_no'],
      booking_no:     body['booking_no']
    }, {
      status // FIXME: Status จะ completed ต่อเมื่อ import ทั้ง frieght และ document แล้ว
    })
    
    let eta_port_of_destination = moment(frieghtView.ETA, 'DD-MMM-YY')

    let actual_eta = null
    let on_time = null

    if (body['actual_eta'] && body['actual_eta'] !== '') {
      const actual_eta_moment = moment(body['actual_eta'], 'DD/MM/YYYY')
      actual_eta = actual_eta_moment.toDate()
      on_time = actual_eta_moment.diff(eta_port_of_destination, 'days')
    }
    
    await fastify.service(fastify.oracle).upsertFrieght({
      booking_no: body['booking_no'],
      delivery_no: body['delivery_no']
    },{
      border: body['border'],
      month: body['month'],
      liner: body['line_fwd'],
      fwd: body['line_fwd'],
      etd_port_of_loading: body['etd_port_of_loading'] ? moment(body['etd_port_of_loading'], 'DD/MM/YYYY').toDate() : null,
      actual_etd_port_of_loading: body['actual_etd_port_of_loading'] ? moment(body['actual_etd_port_of_loading'], 'DD/MM/YYYY').toDate() : null,
      feeder_name: body['feeder_name'],
      port_of_transhipment: body['port_of_transhipment'],
      eta_port_of_transhipment: body['eta_port_of_transhipment'] ? moment(body['eta_port_of_transhipment'], 'DD/MM/YYYY').toDate() : null,
      etd_port_of_transhipment: body['etd_port_of_transhipment'] ? moment(body['etd_port_of_transhipment'], 'DD/MM/YYYY').toDate() : null,
      mother_vessel_name: body['mother_vessel_name'],
      on_time, 
      delivery_on_time: fastify.parser.getDeliveryOnTimeFrieght(on_time),
      frieght_usd: body['usd'],
      VGM: body['vgm'],
      invoice: body['invoice'],
      '"DATE"': body['date'] ? moment(body['date'], 'DD/MM/YYYY').toDate() : null,
      due: body['due'] ? moment(body['due'], 'DD/MM/YYYY').toDate() : null,
      FRIEGHT_URL: body['url'],
      shipment_status,
      actual_etd: body['actual_etd'] ? moment(body['actual_etd'], 'DD/MM/YYYY').toDate() : null,
      responsibility: body['responsibility'],
      actual_eta,
      no_postpone: body['no_postpone'],
      remark: body['remark'],
      reason: body['reason'],
      baht: (typeof body['baht'] == "string") ? body['baht'].replace(',', '') : body['baht'],
      myr: (typeof body['myr'] == "string") ? body['myr'].replace(',', '') : body['myr'],
      place_bkg_date: dateFromCsvToOracleFormat(body['place_bkg_date']),
      release_bkg_date: dateFromCsvToOracleFormat(body['release_bkg_date']),
      reason_booking: body['reason_booking'],
      freetime: body['freetime'],
      reference: body['reference']
    })

    return { message: 'Upsert frieght data success!' }
  })
}

module.exports.autoPrefix = '/frieght'