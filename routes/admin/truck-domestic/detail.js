'use strict' 

const axios = require('axios')
const moment = require('moment')

module.exports = async (fastify, opts) => { 
  fastify.get('/:deliveryNo/:deliveryItem', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  }, async (request) => {
    const { params, query } = request
    const response = await fastify.service(fastify.oracle).getTruckDomesticDetail(params.deliveryNo, params.deliveryItem, query)
    let items = []
    for (let t of response) {
      if (t.PLANT === 'PNP1') {
        const { DO: delivery, SO_ITEM: item } = t
        const responseTms = await axios.get('http://dev.panelplus.co.th/logistics_v2/model/get_vehicle_detail.php', { params: {delivery, item} })
        t.THAI_DRIVER = responseTms.data ? responseTms.data[0].driver : null
        t.THAI_TRUCK_NO = responseTms.data ? responseTms.data[0].car_id : null
        t.PHONE = t.PHONE ? t.PHONE : (responseTms.data ? responseTms.data[0].phone : null)
        t.TRANSPORTATION_NAME = t.TRANSPORTATION_NAME ? t.TRANSPORTATION_NAME : (responseTms.data ? responseTms.data[0].supplier_name : null)
        t.CARGO_DEPARTURE_FROM_PNP_DATE = responseTms.data && responseTms.data[0].atd ? moment(responseTms.data[0].atd.date) : null
        t.CARGO_DEPARTURE_FROM_PNP_TIME = responseTms.data && responseTms.data[0].actual_time_out ? moment(responseTms.data[0].actual_time_out.date).format("HH:mm") : null
        t.ESTIMATE_ARRIVAL_DATE = responseTms.data && responseTms.data[0].eta ? moment(responseTms.data[0].eta.date) : null
        t.STATUS = t.ESTIMATE_ARRIVAL_DATE ? 'completed' : 'pending'
        t.ESTIMATE_ARRIVAL_TIME = responseTms.data && responseTms.data[0].eta ? moment(responseTms.data[0].eta.date).format("HH:mm") : null
      } else {
        t.ESTIMATE_ARRIVAL_DATE = t.ESTIMATE_ARRIVAL_DATE ? moment(t.ESTIMATE_ARRIVAL_DATE, 'DD/MM/YYYY') : null
      }
      
      items.push(t)
    }
    return items
  })
}

module.exports.autoPrefix = '/truck-domestic'