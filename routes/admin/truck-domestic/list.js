'use strict' 

const axios = require('axios')
const moment = require('moment')

module.exports = async (fastify, opts) => { 
  fastify.get('/', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  }, async (request) => {
    const { query } = request
    const response = await fastify.service(fastify.oracle).getTruckDomesticList(query)
    
    let responseTms
    let items = []
    for (let t of response.items) {
      const { DO: delivery, SO_ITEM: item } = t
      t.THAI_DRIVER = null
      t.THAI_TRUCK_NO = null

      if (t.PLANT === 'PNP1') {
        try {
          responseTms = await axios.get('http://dev.panelplus.co.th/logistics_v2/model/get_vehicle_detail.php', { 
            headers: {
              'Content-Type': 'application/json'
            }, 
            params: {delivery, item} 
          })
        } catch (e) {
          console.log(e)
        }
        
        t.THAI_DRIVER = responseTms.data ? responseTms.data[0].driver : null
        t.THAI_TRUCK_NO = responseTms.data ? responseTms.data[0].car_id : null
        t.CARGO_DEPARTURE_FROM_PNP_DATE_IN = responseTms.data && responseTms.data[0].atd ? moment(responseTms.data[0].atd.date) : null
        t.CARGO_DEPARTURE_FROM_PNP_TIME_OUT = responseTms.data && responseTms.data[0].actual_time_out ? moment(responseTms.data[0].actual_time_out.date).format("HH:mm") : null
        t.CARGO_ARRIVAL_TO_PNP_TIME_IN = responseTms.data && responseTms.data[0].actual_time_in ? moment(responseTms.data[0].actual_time_in.date).format("HH:mm") : null
      }
      
      items.push(t)
    }

    response.items = items
    return response
  })
}

module.exports.autoPrefix = '/truck-domestic'