'use strict' 

const moment = require('moment')

module.exports = async (fastify, opts) => { 
  fastify.put('/', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  }, async (request, reply) => {
    const { body } = request

    body['ESTIMATE_ARRIVAL_DATE'] = moment(body['ESTIMATE_ARRIVAL_DATE']).isValid() ? moment(body['ESTIMATE_ARRIVAL_DATE']).format('DD/MM/YYYY') : body['ESTIMATE_ARRIVAL_DATE']
    
    await fastify.service(fastify.oracle).upsertTruckDomestic({
      do_no: body['DO'],
      item: body['SO_ITEM']
    },{
      truck_type: body['TRUCK_TYPE'],
      transportation_name: body['TRANSPORTATION_NAME'],
      warehouse_name: body['WAREHOUSE_NAME'],
      location: body['LOCATION'],
      estimate_arrival_date: body['ESTIMATE_ARRIVAL_DATE'],
      estimate_arrival_time: body['ESTIMATE_ARRIVAL_TIME'],
      phone: body['PHONE']
    })

    return { message: 'Upsert truck domestic data success!' }
  })
}

module.exports.autoPrefix = '/truck-domestic'