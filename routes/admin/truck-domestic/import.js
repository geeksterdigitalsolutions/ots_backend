'use strict' 

const moment = require('moment')
const csv = require('csvtojson')

module.exports = async (fastify, opts) => { 
  fastify.post('/', {
    preValidation: [
      await fastify.oauth.verifyAccessToken
    ]
  }, async (request, reply) => {
    try {
      const { truck_domestic_csv } = request.raw.files
      // const { dateFromCsvToOracleFormat } = fastify.parser
      let truckDomesticCSVData = await csv({ noheader:true }).fromString(truck_domestic_csv.data.toString())
      
      truckDomesticCSVData.shift()

      // const errors = await fastify.validator.truckImportValidate(truckCSVData)
      // if (errors.length > 0) {
      //   return reply.code(400).send({ errors })
      // }
      console.log(truckDomesticCSVData)
      for (let td of truckDomesticCSVData) {
        await fastify.service(fastify.oracle).upsertTruckDomestic({
          do_no: td['field2'],
          item: td['field3']
        }, {
          truck_type: td['field15'],
          transportation_name: td['field16'],
          warehouse_name: td['field17'],
          location: td['field18'],
          estimate_arrival_date: td['field11'],
          estimate_arrival_time: td['field12'],
          phone: td['field19']
        })
      }
      
      return { message: 'Import success!' }
    } catch (e) {
      console.log(e)
      return reply.code(500).send({ message: e.message })
    }
  })
}

module.exports.autoPrefix = '/truck-domestic'