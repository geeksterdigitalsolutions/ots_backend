'use strict' 
const moment = require('moment')

const compressDateTime = (date, time) => {
  return date !== null && time !== null ? moment(`${date} ${time}`): null
}

module.exports = async (fastify, opts) => { 
  fastify.get('/truck-domestic/:do_no', {
    preValidation: [
      // fastify.oauth.verifyAccessToken
    ]
  },async (request) => {
    const { params } = request
    let data = fastify.routeUtils.make('PNP HY Factory', 'Customer')
    
    let result = await fastify.service(fastify.oracle).getRouteTruckDomestic(params.do_no)
    // return result
    if (result.length == 0) {
      return data  
    } else {
      result = result[0]
      const ACTUAL_ETA = compressDateTime(result.ACTUAL_ETA_DATE, result.ACTUAL_ETA_TIME)

      const factoryPlan = result.ETA ? moment(result.ETA).subtract(2, 'd') : null
      data = [
        {
          name: 'PNP HY Factory',
          plan: factoryPlan,
          actual: ACTUAL_ETA,
          status: ACTUAL_ETA != null,
        },
        // {
        //   name: 'PNP SK Factory',
        //   plan: factoryPlan,
        //   actual: ACTUAL_ETA,
        //   status: ACTUAL_ETA != null,
        // },
        {
          name: 'Customer',
          plan: ACTUAL_ETA ? ACTUAL_ETA.add(2, 'd') : factoryPlan.add(2, 'd'),
          actual: null,
          status: false,
        },
      ]
      return data  
    }

  })
  
}