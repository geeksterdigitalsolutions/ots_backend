'use strict' 

module.exports = async (fastify, opts) => { 
  fastify.get('/truck-my/:do_no/:so_no_ref', {
    preValidation: [
      // fastify.oauth.verifyAccessToken
    ]
  },async (request) => {
    const { params } = request
    
    let result = await fastify.service(fastify.oracle).getRouteTruckMy(params.do_no, params.so_no_ref)
    // return result
    if (!result) {
      return []  
    } else {
      if (result.COMMERCIAL_TERM === 'DAP' && result.PORT_DESC.startsWith('Bukit')) {
        return fastify.routeUtils.mapRouteExportPNPBorder(result)
      } else if (result.COMMERCIAL_TERM === 'EXW' && result.PORT_DESC.indexOf('Malaysia') !== -1) {
        return fastify.routeUtils.mapRouteExportPNPHYFactory(result)
      } else if (result.COMMERCIAL_TERM === 'EXW' && ['Cambodia', 'Laos', 'Myanmar', 'Vietnam'].some(country => result.PORT_DESC.indexOf(country) !== -1)) {
        return fastify.routeUtils.mapRouteExportPNPCLMVToSK(result)
      } else if (result.COMMERCIAL_TERM === 'DAP' && ['Cambodia', 'Laos', 'Myanmar', 'Vietnam'].some(country => result.PORT_DESC.indexOf(country) !== -1)) {
        return fastify.routeUtils.mapRouteExportPNPCLMVToDAP(result)
      } else if (result.COMMERCIAL_TERM === 'DAP' && !result.PORT_DESC.startsWith('Bukit')) {
        return fastify.routeUtils.mapRouteExportPNPCustomer(result)
      } 
      return []
    }
  })
  
}