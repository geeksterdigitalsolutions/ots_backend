'use strict' 

const axios = require('axios')

module.exports = async (fastify, opts) => { 
  fastify.get('/tms/:do/:so_item', {
    preValidation: [
      fastify.oauth.verifyCustomerAccessToken
    ]
  }, async (request) => {
    const { params } = request
    const responseTms = await axios.get('http://dev.panelplus.co.th/logistics_v2/model/get_vehicle_detail.php', { params: { delivery: params.do, item: params.so_item } })
    
    return {
      THAI_DRIVER: responseTms.data ? responseTms.data[0].driver : null,
      THAI_TRUCK_NO: responseTms.data ? responseTms.data[0].car_id : null
    }
  })
}

module.exports.autoPrefix = '/truck'