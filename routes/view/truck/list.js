'use strict' 

const axios = require('axios')

module.exports = async (fastify, opts) => { 
  fastify.get('/', {
    preValidation: [
      fastify.oauth.verifyCustomerAccessToken
    ]
  }, async (request) => {
    const { query, user } = request

    query.CUSTOMER_CODE = user.code
    const response = await fastify.service(fastify.oracle).getTruckViewList(query)
    return response
  })
}

module.exports.autoPrefix = '/truck'