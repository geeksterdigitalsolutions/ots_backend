'use strict' 

const axios = require('axios')

module.exports = async (fastify, opts) => { 
  fastify.get('/document/:so/:do_no/:item', {
    preValidation: [
      // fastify.oauth.verifyAccessToken
    ]
  }, async (request) => {
    const { params } = request
    const response = await fastify.service(fastify.oracle).getDocumentDetailHY(params.so, params.do_no, params.item)
    return response
  })
}

module.exports.autoPrefix = '/truck'