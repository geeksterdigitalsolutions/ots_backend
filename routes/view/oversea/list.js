'use strict' 

const moment = require('moment')

module.exports = async (fastify, opts) => { 
  fastify.get('/', {
    preValidation: [
      fastify.oauth.verifyCustomerAccessToken
    ]
  },async (request) => {
    const { query, user } = request
    
    query.CUSTOMER_CODE = user.code
    const response = await fastify.service(fastify.oracle).getOverseaList(query) 
    return response
  })
}

module.exports.autoPrefix = '/document'