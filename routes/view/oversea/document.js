'use strict' 

const moment = require('moment')

module.exports = async (fastify, opts) => { 
  fastify.get('/document/:booking_no/:delivery_no', {
    preValidation: [
      // fastify.oauth.verifyAccessToken
    ]
  },async (request) => {
    const { params } = request
    
    params.delivery_no = ['undefined', 'null'].includes(params.delivery_no) ? null : params.delivery_no

    const response = await fastify.service(fastify.oracle).getDocumentDetail(params.booking_no, params.delivery_no) 
    const documentConfig = await fastify.service(fastify.oracle).getDocumentConfig()

    return response.map(res => {
      const item = {
        default_link: documentConfig.default_link,
        TRANS_TYPE: res.TRANS_TYPE,
        DO_NO: res.DO_NO,
        SO_NO: res.SO_NO,
        COMMERCIAL_INVOICE: res.COMMERCIAL_INVOICE,
        CUSTOMER_NAME: res.CUSTOMER_NAME,
        BOOKING_NO: res.BOOKING_NO,
        DESTINATION_PORT: res.DESTINATION_PORT,
        PAYMENT_TERM: res.PAYMENT_TERM,
        SHIP_DATE: res.SHIP_DATE,
        FWD: res.FWD,
        LINER: res.LINER,
        ETD_PORT_OF_LOADING: res.ETD_PORT_OF_LOADING,
        ETA_PORT_OF_DESTINATION: res.ETA_PORT_OF_DESTINATION,
        SUM_OF_CONTAINER: res.SUM_OF_CONTAINER,
        RESPONSIBILITY: res.RESPONSIBILITY,
        FORM: res.FORM,
        LC_NO: res.LC_NO,
        RECEIVE_LC: res.RECEIVE_LC,
        LC_EXPIRY: res.LC_EXPIRY,
        LDS: res.LDS,
        PERIOD_OF_PRESENTION: res.PERIOD_OF_PRESENTION,
        BL_NO: res.BL_NO,
        TRACKING_NO: res.TRACKING_NO,
        COURIER_BY: res.COURIER_BY
      }

      if (res.DISPLAY_CBR == 1) {
        item['CBR_PLAN'] = moment(res['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').subtract(2, 'days').format('DD-MMM-YY')
        item['CBR_ACTUAL'] = res.CBR_ACTUAL
      }
      if (res.DISPLAY_SI == 1) {
        item['SI_PLAN'] = moment(res['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').subtract(2, 'days').format('DD-MMM-YY')
        item['SI_ACTUAL'] = res.SI_ACTUAL
      }
      if (res.DISPLAY_DRAFT_BL == 1) {
        item['DRAFT_BL_PLAN'] = moment(res['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').subtract(1, 'days').format('DD-MMM-YY')
        item['DRAFT_BL_ACTUAL'] = res.DRAFT_BL_ACTUAL
      }
      if (res.DISPLAY_CONFIRM_BL == 1) {
        item['CONFIRM_BL_PLAN'] = moment(res['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').format('DD-MMM-YY')
        item['CONFIRM_BL_ACTUAL'] = res.CONFIRM_BL_ACTUAL
      }
      if (res.DISPLAY_OBL_BL == 1) {
        item['OBL_PLAN'] = moment(res['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').add(1, 'days').format('DD-MMM-YY')
        item['OBL_ACTUAL'] = res.OBL_ACTUAL
      }
      if (res.DISPLAY_APPLY_PSYTO == 1) {
        item['APPLY_PSYTO_PLAN'] = moment(res['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').format('DD-MMM-YY')
        item['APPLY_PSYTO_ACTUAL'] = res.APPLY_PSYTO_ACTUAL
      }
      if (res.DISPLAY_RECEIVE_PSYTO == 1) {
        item['RECEIVE_PSYTO_PLAN'] = moment(res['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').format('DD-MMM-YY')
        item['RECEIVE_PSYTO_ACTUAL'] = res.RECEIVE_PSYTO_ACTUAL
      }
      if (res.DISPLAY_APPLY_COCI_CHAMBER == 1) {
        item['APPLY_COCI_CHAMBER_PLAN'] = moment(res['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').format('DD-MMM-YY')
        item['APPLY_COCI_CHAMBER_ACTUAL'] = res.APPLY_COCI_CHAMBER_ACTUAL
      }
      if (res.DISPLAY_RECEIVE_COCI_CHAMBER == 1) {
        item['RECEIVE_COCI_CHAMBER_PLAN'] = moment(res['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').add(2, 'days').format('DD-MMM-YY')
        item['RECEIVE_COCI_CHAMBER_ACTUAL'] = res.RECEIVE_COCI_CHAMBER_ACTUAL
      }
      if (res.DISPLAY_APPLY_FTA_FORM_PLAN == 1) {
        item['APPLY_FTA_FORM_PLAN'] = moment(res['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').format('DD-MMM-YY')
        item['APPLY_FTA_FORM_ACTUAL'] = res.APPLY_FTA_FORM_ACTUAL
      }
      if (res.DISPLAY_RECEIVE_FTA_FORM_PLAN == 1) {
        item['RECEIVE_FTA_FORM_PLAN'] = moment(res['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').add(2, 'days').format('DD-MMM-YY')
        item['RECEIVE_FTA_FORM_ACTUAL'] = res.RECEIVE_FTA_FORM_ACTUAL
      }
      if (res.DISPLAY_BE_PLAN == 1) {
        item['BE_PLAN'] = moment(res['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').format('DD-MMM-YY')
        item['BE_ACTUAL'] = res.BE_ACTUAL
      }
      if (res.DISPLAY_SCAN_SENT_EMAIL_PLAN == 1) {
        item['SCAN_SENT_EMAIL_PLAN'] = moment(res['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').format('DD-MMM-YY')
        item['SCAN_SENT_EMAIL_ACTUAL'] = res.SCAN_SENT_EMAIL_ACTUAL
      }
      if (res.DISPLAY_COMPLETE_SET_PLAN == 1) {
        item['COMPLETE_SET_PLAN'] = moment(res['ETD_PORT_OF_LOADING'], 'DD-MMM-YY').add(3, 'days').format('DD-MMM-YY')
        item['COMPLETE_SET_ACTUAL'] = res.COMPLETE_SET_ACTUAL
      }

      return item
    })
  })
}