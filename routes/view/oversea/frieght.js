'use strict' 

const moment = require('moment')

module.exports = async (fastify, opts) => { 
  fastify.get('/frieght/:booking_no/:delivery_no', {
    preValidation: [
      // fastify.oauth.verifyAccessToken
    ]
  },async (request) => {
    const { params } = request
    
    const response = await fastify.service(fastify.oracle).getFrightDetail(params.booking_no, params.delivery_no)
    return response
  })
}