'use strict' 

const moment = require('moment')

module.exports = async (fastify, opts) => { 
  fastify.get('/container/:booking_no', {
    preValidation: [
      // fastify.oauth.verifyAccessToken
    ]
  },async (request) => {
    const { params } = request
    
    const response = await fastify.service(fastify.oracle).getContainerDetail(params.booking_no) 
    
    return response
  })
}