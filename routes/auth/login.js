'use strict'

module.exports = async (fastify, opts) => {
  fastify.post('/login', {
    schema: {
      body: {
        type: 'object',
        properties: {
          username: { type: 'string' },
          password: { type: 'string' },
          role: { type: 'string', enum: ['employee', 'customer'] }
        }
      }
    }
  }, async (request, reply) => {
    const { username, password, role } = request.body
    if (role === 'employee') {
      const response = await fastify.oauth.getAccessToken({ username, password })
      const { status } = response
      if (status !== 200) return reply.code(status).send({ message: response.message })

      const { access_token, refresh_token } = response
      const userProfile = await fastify.oauth.getUserProfile(access_token)
      return { ...userProfile, access_token, refresh_token }
    } else if (role === 'customer') {
      try {
        const response = await fastify.oauth.customerLogin({ username, password })
        return response
      } catch (e) {
        console.log(e)
        return reply.code(401).send({ message: 'Invalid credential' })
      }
      
    }

    return { message: 'no role' }
  })
}

module.exports.autoPrefix = '/auth'