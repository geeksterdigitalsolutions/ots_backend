'use strict'

module.exports = async (fastify, opts) => {
  fastify.post('/refresh_token', {
    schema: {
      body: {
        type: 'object',
        properties: {
          refresh_token: { type: 'string' }
        }
      }
    }
  }, async (request, reply) => {
    const { refresh_token: refreshToken } = request.body
    const response = await fastify.oauth.refreshToken(refreshToken)

    const { status } = response
    if (status !== 200) return reply.code(status).send({ message: response.message })

    const { access_token, refresh_token } = response
    const userProfile = await fastify.oauth.getUserProfile(access_token)
    return { ...userProfile, access_token, refresh_token }
  })
}

module.exports.autoPrefix = '/auth'