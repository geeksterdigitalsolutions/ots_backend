'use strict' 

module.exports = async (fastify, opts) => { 
  fastify.get('/customers', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  },async (request) => {
    return fastify.service(fastify.oracle).getMasterCustomers()
  })
}

module.exports.autoPrefix = '/master'