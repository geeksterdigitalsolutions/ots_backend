'use strict' 

module.exports = async (fastify, opts) => { 
  fastify.get('/responsibility', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  },async (request) => {
    const response = await fastify.service(fastify.oracle).getConfig('RESPONSIBILITY')
    return JSON.parse(response)
  })

  fastify.put('/responsibility', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  },async (request) => {
    await fastify.service(fastify.oracle).updateConfig('RESPONSIBILITY', JSON.stringify(request.body))
    return { message: 'update successful'}
  })
}

module.exports.autoPrefix = '/master'