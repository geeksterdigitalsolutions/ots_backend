'use strict' 

module.exports = async (fastify, opts) => { 
  fastify.get('/status', {
    preValidation: [
      // fastify.oauth.verifyAccessToken
    ]
  },async (request) => {
    return ['pending', 'completed']
  })
}

module.exports.autoPrefix = '/master'