'use strict' 

module.exports = async (fastify, opts) => { 
  fastify.get('/destinations', {
    preValidation: [
      fastify.oauth.verifyAccessToken
    ]
  },async (request) => {
    return fastify.service(fastify.oracle).getDestinations()
  })
}

module.exports.autoPrefix = '/master'