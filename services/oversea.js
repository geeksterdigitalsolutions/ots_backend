const oracleSql = require('./oracle.sql')
const moment = require('moment')

module.exports = (oracleConnection) => {
  const { query, upsert, pagination, parseFilter } = oracleSql(oracleConnection)
  return {
    getOverseaList: async (options = {}) => {
      const filters = {}
      if (options.BL_NO && options.BL_NO != '') {
        filters['d.BL_NO'] = options.BL_NO
      }
      if (options.BOOKING_NO && options.BOOKING_NO != '') {
        filters['vf.BOOKING_NO'] = options.BOOKING_NO
      }
      if (options.PO_NO && options.PO_NO != '') {
        filters['vf.PO_NO'] = options.PO_NO
      }
      if (options.CI_NO && options.CI_NO != '') {
        filters['vf.CI_NO'] = options.CI_NO
      }
      if (options.CUSTOMER_CODE && options.CUSTOMER_CODE != '') {
        filters['vf.CUSTOMER_CODE'] = options.CUSTOMER_CODE
      }

      if (Object.keys(filters).length == 0) {
        filters['vf.ETD_PORT'] = {
          from: moment().subtract(1, 'M').startOf('month').format('YYYY-MM-DD'),
          to: moment().add(1, 'M').endOf('month').format('YYYY-MM-DD'),
        }
      }

      const filtersCommand = parseFilter(filters)
      const sql = `
      SELECT
        d.bl_no,
        d.tracking_no,
        d.courier_by,
        vf.delivery_no,
        vf.booking_no,
        vf.ci_no,
        vf.customer_code,
        vf.ctn_size,
        vf.po_no,
        c."SIZE",
        vf.vol,
        vf.destination_port,
        vf.payment_status,
        f.shipment_status,
        d.status_of_document, 
        vf.etd_port,
        vf.eta,
        f.frieght_url,
        row_number() over (ORDER BY vf.etd_port desc) line_number
      FROM v_spt_freight vf
      LEFT JOIN ots_document d ON d.booking_no = vf.booking_no AND vf.delivery_no = d.delivery_no
      LEFT JOIN (SELECT
        booking_no,
        MIN("SIZE") KEEP(dense_rank first order by booking_no) as "SIZE"
        FROM ots_container GROUP BY booking_no) c ON c.booking_no = vf.booking_no 
      LEFT JOIN ots_frieght f ON f.booking_no = vf.booking_no AND f.delivery_no = vf.delivery_no ${filtersCommand}`
      
      return await pagination(sql, options)
    },
  }
}