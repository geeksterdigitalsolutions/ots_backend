
const oracleSql = require('./oracle.sql')

module.exports = (oracleConnection) => {
  const { query, upsert, pagination, parseFilter } = oracleSql(oracleConnection)

  return {
    getAdminContainerList: async (options) => {
      const sort = {
        name: options.sort_name || 'BOOKING_NO',
        by: options.sort_by || 'DESC'
      }

      const filters = {}
      if (options.BOOKING_NO && options.BOOKING_NO != '') {
        console.log(options.BOOKING_NO)
        filters['c.BOOKING_NO'] = options.BOOKING_NO.toUpperCase()
      }
      if (options.VENDOR && options.VENDOR != '') {
        filters['c.VENDOR'] = options.VENDOR
      }
      if (options.DEPOT && options.DEPOT != '') {
        filters['c.DEPOT'] = options.DEPOT
      }
      if (options.SHIPPING_AGENT && options.SHIPPING_AGENT != '') {
        filters['c.SHIPPING_AGENT'] = options.SHIPPING_AGENT
      }

      const filtersCommand = parseFilter(filters)

      const sql = `SELECT
        c.booking_no,
        MIN(c.vol) KEEP(dense_rank first order by c.booking_no) as vol,
        MIN(c.depot) KEEP(dense_rank first order by c.booking_no) as depot,
        MIN(c.closing_date) KEEP(dense_rank first order by c.booking_no) as closing_date,
        MIN(c.closing_time) KEEP(dense_rank first order by c.booking_no) as closing_time,
        MIN(c.free_time) KEEP(dense_rank first order by c.booking_no) as free_time,
        MIN(c.vessel) KEEP(dense_rank first order by c.booking_no) as vessel,
        MIN(c."SIZE") KEEP(dense_rank first order by c.booking_no) as "size",
        SUM(CASE WHEN c.EMPTY_CONTAINER_DEPOT_NO IS NOT NULL THEN 1 ELSE 0 END) as SUM_EMPTY_CONTAINER,
        SUM(CASE WHEN c.FORWARDING_RELEASE_PORT_DATE IS NOT NULL THEN 1 ELSE 0 END) as SUM_LADEN_RETURN,
        row_number() over (ORDER BY c.${sort.name} ${sort.by}) line_number
      FROM ots_container c ${filtersCommand}
      GROUP BY c.booking_no`

      return await pagination(sql, options)
    },
    getAdminContainerListDetail: async (options) => {
      const sort = {
        name: options.sort_name || 'BOOKING_NO',
        by: options.sort_by || 'DESC'
      }

      const filters = {}
      if (options.BOOKING_NO) {
        filters['c.BOOKING_NO'] = options.BOOKING_NO
      }
      if (options['!BOOKING_NO']) {
        filters['!c.BOOKING_NO'] = options['!BOOKING_NO']
      }
      if (options.VENDOR && options.VENDOR != '') {
        filters['c.VENDOR'] = options.VENDOR
      }

      const filtersCommand = parseFilter(filters)

      const sql = `SELECT
        c.*,
        vf.ci_no,
        ivd.item_desc,
        ivd.sheet_qty,
        ivd.cbm_qty,
        row_number() over (ORDER BY c.${sort.name} ${sort.by}) line_number
      FROM ots_container c
      LEFT JOIN v_spt_freight vf ON vf.booking_no = c.booking_no
      LEFT JOIN iv_detail ivd ON ivd.iv_no = vf.ci_no AND ivd.container_no = c.empty_container_depot_no
      ${filtersCommand}`

      return await pagination(sql, options)
    },
    getContainerDetail: async (booking_no) => {
      const sql = `SELECT
        c.*,
        vf.ci_no,
        vf.delivery_no,
        ivd.item_desc,
        ivd.sheet_qty,
        ivd.cbm_qty
      FROM ots_container c
      LEFT JOIN v_spt_freight vf ON vf.booking_no = c.booking_no
      LEFT JOIN iv_detail ivd ON ivd.iv_no = vf.ci_no AND ivd.container_no = c.empty_container_depot_no
      WHERE c.booking_no = '${booking_no}'
      ORDER BY NO ASC`
      
      return await query(sql)
    },
    upsertContainer: async (primary, data) => {
      return await upsert('ots_container', primary, data)
    },
    getAdminContainerDashboard: async () => {
      const sql = `SELECT
        SUM (
          CASE 
            WHEN depot IS NOT NULL AND empty_container_depot_date IS NULL AND empty_container_out_th_date IS NULL AND laden_come_back_to_yard_date IS NULL AND laden_container_out_port_date IS NULL THEN 1
            ELSE 0
          END
        ) AS "depot",
        SUM (
          CASE 
            WHEN empty_container_depot_date IS NOT NULL AND empty_container_out_th_date IS NULL AND laden_come_back_to_yard_date IS NULL AND laden_container_out_port_date IS NULL THEN 1
            ELSE 0
          END
        ) AS "empty_container_depot_penang",
        SUM (
          CASE 
            WHEN empty_container_out_th_date IS NOT NULL AND laden_come_back_to_yard_date IS NULL AND laden_container_out_port_date IS NULL THEN 1
            ELSE 0
          END
        ) AS "empty_container_out_thailand",
        SUM (
          CASE 
            WHEN laden_come_back_to_yard_date IS NOT NULL AND laden_container_out_port_date IS NULL THEN 1
            ELSE 0
          END
        ) AS "laden_come_back_yard",
        SUM (
          CASE 
            WHEN laden_container_out_port_date IS NOT NULL THEN 1
            ELSE 0
          END
        ) AS "laden_container_out_penang"
        FROM ots_container`
      return query(sql)
    }
  }
}