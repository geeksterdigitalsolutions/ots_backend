const oracleSql = require('./oracle.sql')

module.exports = (oracleConnection) => {
  const { query, upsert, pagination, parseFilter } = oracleSql(oracleConnection)

  return {
    getAdminDocumentList: async (options) => {
      const sort = {
        name: options.sort_name || 'ETD_PORT_OF_LOADING',
        by: options.sort_by || 'DESC'
      }

      const filters = {
        'v.TRANS_TYPE': 'S'
      }
      if (options.SO_NO && options.SO_NO != '') {
        filters['v.SO_NO'] = options.SO_NO
      }
      if (options.DO_NO && options.DO_NO != '') {
        filters['v.DO_NO'] = options.DO_NO
      }
      if (options.COMMERCIAL_INVOICE && options.COMMERCIAL_INVOICE != '') {
        filters['v.IV_NO'] = options.COMMERCIAL_INVOICE
      }
      if (options.BOOKING_NO && options.BOOKING_NO != '') {
        filters['v.BOOKING_NO'] = options.BOOKING_NO
      }
      if (options.DESTINATION_PORT && options.DESTINATION_PORT != '') {
        filters['v.DEST_PORT'] = options.DESTINATION_PORT
      }
      if (options.PAYMENT_TERM && options.PAYMENT_TERM != '') {
        filters['v.PAYMENT_TERM'] = options.PAYMENT_TERM
      }
      if (options.SHIP_DATE) {
        filters['v.SHIP_DATE'] = options.SHIP_DATE
      }
      if (options.FWD && options.FWD != '') {
        filters['vf.LINER_NAME'] = options.FWD
      }
      if (options.LINER && options.LINER != '') {
        filters['vf.LINER_NAME'] = options.LINER
      }
      if (options.RESPONSIBILITY && options.RESPONSIBILITY != '') {
        filters['d.RESPONSIBILITY'] = options.RESPONSIBILITY
      }
      if (options.DOCUMENT_STATUS && options.DOCUMENT_STATUS != '') {
        filters['d.status_of_document'] = options.DOCUMENT_STATUS === 'pending' ? '!completed' : 'completed'
      }
      if (options.ETD_PORT_OF_LOADING) {
        filters['v.etd'] = options.ETD_PORT_OF_LOADING
      }
      if (options.ETA_PORT_DESTINATION) {
        filters['v.eta'] = options.ETA_PORT_DESTINATION
      }

      if (Object.keys(filters).length == 0) {
        filters['v.etd'] = {
          from: moment().subtract(1, 'M').startOf('month').format('YYYY-MM-DD'),
          to: moment().add(1, 'M').endOf('month').format('YYYY-MM-DD'),
        }
      }

      const filtersCommand = parseFilter(filters)

      const sql = `
        SELECT
          d.*,
          row_number() over (ORDER BY etd_port_of_loading DESC) line_number
        FROM (
          SELECT DISTINCT
            v.trans_type,
            v.do_no,
            v.so_no,
            v.iv_no as commercial_invoice,
            v.cust_name as customer_name,
            v.booking_no,
            v.dest_port as destination_port,
            v.payment_term,
            v.ship_date,
            vf.liner_name as fwd,
            vf.liner_name as liner,
            v.etd as etd_port_of_loading,
            v.eta as eta_port_of_destination,
            v.total_container as sum_of_container,
            d.responsibility,
            d.form,
            d.lc_no,
            d.receive_lc,
            d.lc_expiry,
            d.lds,
            d.period_of_presentation as period_of_presention,
            d.cbr_actual,
            d.si_actual,
            d.draft_bl_actual,
            d.confirm_bl_actual,
            d.obl_actual,
            d.apply_psyto_actual,
            d.receive_psyto_actual,
            d.apply_coci_chamber_actual,
            d.receive_coci_chamber_actual,
            d.apply_fta_form_actual,
            d.receive_fta_form_actual,
            d.be_actual,
            d.scan_sent_email_actual,
            d.complete_set_actual,
            d.status_remark,
            d.courier_date,
            d.courier_by,
            d.tracking_no,
            d.status_of_document,
            d.total_days_spent,
            d.bank_ref_no,
            d.sst_actual,
            d.permit_actual,
            d.no_of_pending_day,
            d.sales_person,
            d.bl_no
          FROM v_spt_document v
          LEFT JOIN v_spt_freight vf ON v.booking_no = vf.booking_no AND v.do_no = vf.delivery_no
          LEFT JOIN ots_document d ON v.do_no = d.delivery_no and v.booking_no = d.booking_no ${filtersCommand}
        ) d`
        
        return await pagination(sql, options)
    },
    getDocumentDetail: async (booking_no, delivery_no) => {
      const sql = `SELECT DISTINCT
        v.trans_type,
        v.do_no,
        v.so_no,
        v.iv_no as commercial_invoice,
        v.cust_name as customer_name,
        v.booking_no,
        v.dest_port as destination_port,
        v.payment_term,
        v.ship_date,
        vf.liner_name as fwd,
        vf.liner_name as liner,
        v.etd as etd_port_of_loading,
        v.eta as eta_port_of_destination,
        v.total_container as sum_of_container,
        d.responsibility,
        d.form,
        d.lc_no,
        d.receive_lc,
        d.lc_expiry,
        d.lds,
        d.period_of_presentation as period_of_presention,
        d.bl_no,
        d.cbr_actual,
        d.si_actual,
        d.draft_bl_actual,
        d.confirm_bl_actual,
        d.obl_actual,
        d.apply_psyto_actual,
        d.receive_psyto_actual,
        d.apply_coci_chamber_actual,
        d.receive_coci_chamber_actual,
        d.apply_fta_form_actual,
        d.receive_fta_form_actual,
        d.be_actual,
        d.scan_sent_email_actual,
        d.complete_set_actual,
        d.status_remark,
        d.courier_date,
        d.courier_by,
        d.tracking_no,
        d.status_of_document,
        d.total_days_spent,
        d.bank_ref_no,
        d.sst_actual,
        d.permit_actual,
        d.no_of_pending_day,
        d.sales_person,
        d.display_cbr,
        d.display_si,
        d.display_draft_bl,
        d.display_confirm_bl,
        d.display_obl_bl,
        d.display_apply_psyto,
        d.display_receive_psyto,
        d.display_apply_coci_chamber,
        d.display_receive_coci_chamber,
        d.display_apply_fta_form_plan,
        d.display_receive_fta_form_plan,
        d.display_be_plan,
        d.display_scan_sent_email_plan,
        d.display_complete_set_plan
      FROM v_spt_document v
      LEFT JOIN ots_document d ON v.booking_no = d.booking_no AND v.do_no = d.delivery_no
      LEFT JOIN v_spt_freight vf ON v.booking_no = vf.booking_no AND v.do_no = vf.delivery_no
      WHERE v.BOOKING_NO = '${booking_no}'
      AND v.DO_NO = '${delivery_no}'`

      return await query(sql)
    },
    isHasDocument: async (booking_no, delivery_no) => {
      const countSql = `SELECT count(v.booking_no) as "count" FROM v_spt_document v WHERE booking_no = '${booking_no}' AND do_no = '${delivery_no}'`
      const total = await query(countSql)
      return total[0] && total[0].count > 0
    },
    upsertDocument: async (primary, data) => {
      return await upsert('ots_document', primary, data)
    },
    listDocumentConfig: async () => {
      return await query(`SELECT * FROM ots_document_config`)
    },
    getDocumentConfig: async () => {
      const response = await query(`SELECT * FROM ots_document_config`)
      let result = {}
      for (res of response) {
        result[res.NAME] = res.VALUE
      }
      return result
    },
    updateDocumentConfig: async (primary, data) => {
      return await upsert('ots_document_config', primary, data)
    },
  }
}