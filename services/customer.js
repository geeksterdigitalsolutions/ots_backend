
const oracleSql = require('./oracle.sql')
const { promisify } = require('util')
const bcrypt = require('bcrypt')

const hash = promisify(bcrypt.hash)

module.exports = (oracleConnection) => {
  const { query, insert, update, remove, pagination, parseFilter } = oracleSql(oracleConnection)

  return {
    getCustomerList: async (options) => {

      const filters = {}
      if (options.USERNAME && options.USERNAME != '') {
        filters['cust.USERNAME'] = options.USERNAME
      }
      if (options.CUSTOMER_CODE && options.CUSTOMER_CODE != '') {
        filters['cust.CUSTOMER_CODE'] = options.CUSTOMER_CODE
      }
      if (options.AR_NAME && options.AR_NAME != '') {
        filters['v.AR_NAME'] = options.AR_NAME
      }

      const filtersCommand = parseFilter(filters)

      const sql = `
      SELECT
        cust.*,
        v.ar_name,
        row_number() over (ORDER BY cust.created_at DESC) line_number
      FROM ots_customer_account cust
      JOIN v_spt_customer v ON v.ar_code = cust.customer_code ${filtersCommand}
      `
      return await pagination(sql, options)
    },
    getCustomerDetail: async (customer_id) => {

      const sql = `SELECT
        cust.*,
        v.ar_name
      FROM ots_customer_account cust
      JOIN v_spt_customer v ON v.ar_code = cust.customer_code
      WHERE cust.id = ${customer_id}`
      return await query(sql)
    },
    getMasterCustomers: async () => {
      return await query('select * from v_spt_customer')
    },
    createCustomer: async (data) => {
      const response = await query(`SELECT COUNT(*) as total_user FROM ots_customer_account cust`)

      const customer = {
        id: response[0].TOTAL_USER + 1,
        username: data.username,
        password: await hash(data.password, 10),
        customer_code: data.customer_code,
        created_at: new Date(),
        type: data.type
      }
      return await insert('ots_customer_account', customer)
    },
    updateCustomer: async (primary, data) => {
      return await update('ots_customer_account', primary, data)
    },
    deleteCustomer: async (primary) => {
      return await remove('ots_customer_account', primary)
    },
    findByUsername: async (username) => {
      return await query(`SELECT cust.*, v.ar_name FROM ots_customer_account cust JOIN v_spt_customer v ON v.ar_code = cust.customer_code WHERE cust.username = '${username}'`)
    }
  }
}