const oracleSql = require('./oracle.sql')
const moment = require('moment')
const axios = require('axios')

module.exports = (oracleConnection) => {
  const { query, upsert, pagination, parseFilter } = oracleSql(oracleConnection)
  return {
    getRouteContainer: async (booking_no, do_no = null) => {
      const sql = `
        SELECT
          sm.fac_code,
          vf.border_code,
          vf.etd_port,
          vpd.start_picking_date,
          c.*,
          f.actual_etd_port_of_loading as actual_etd,
          f.actual_etd as actual_etd_pot,
          f.etd_port_of_transhipment,
          vf.eta,
          f.actual_eta
        FROM v_spt_freight vf
        LEFT JOIN (
          SELECT 
            booking_no, 
            MAX(laden_come_back_to_yard_date) as laden_come_back_to_yard_date, 
            MAX(laden_container_out_port_date) as laden_container_out_port_date
          FROM ots_container 
          GROUP BY booking_no 
        ) c ON c.booking_no = vf.booking_no
        LEFT JOIN (
          SELECT 
            booking_no, 
            MIN(start_picking_date) as start_picking_date
          FROM v_spt_picking_date 
          GROUP BY booking_no 
        ) vpd ON vpd.booking_no = vf.booking_no
        LEFT JOIN ots_frieght f ON vf.booking_no = f.booking_no
        LEFT JOIN so_master sm ON vf.delivery_no = sm.so_no
        WHERE vf.BOOKING_NO = '${booking_no}'${do_no !== null ? ` AND vf.delivery_no = '${do_no}'` : ''}`
      return await query(sql)
    },
    getRouteTruckMy: async (do_no, so_no_ref) => {
      const sql = `
      SELECT 
        vt.ETD_PORT,
        vt.SO_ITEM,
        vt.PORT_DESC,
        vt.PI_NO,
        sm.COMMERCIAL_TERM,
        t.CARGO_PNP_TO_CROSS_BORDER_DATE,
        t.CARGO_PNP_TO_CROSS_BORDER_TIME,
        t.CARGO_BKHWH_TO_CUSTOMER_DATE,
        t.CARGO_BKHWH_TO_CUSTOMER_TIME,
        t.ESTIMATE_ARRIVAL_DATE,
        t.ESTIMATE_ARRIVAL_TIME
      FROM v_spt_truck_tms vt
      LEFT JOIN so_master sm ON vt.pi_no = sm.so_no AND vt.so_no_ref = sm.so_no_ref
      LEFT JOIN ots_truck t ON vt.pi_no = t.do AND vt.ci_no = t.invoice_no
      where vt.pi_no = '${do_no}' and vt.so_no_ref = '${so_no_ref}'`
      const response = await query(sql)
      
      if (response[0]) {
        try {  
          let responseTms = await axios.get('http://dev.panelplus.co.th/logistics_v2/model/get_vehicle_detail.php', { 
            headers: { 'Content-Type': 'application/json' }, 
            params: {delivery: response[0].PI_NO, item: response[0].SO_ITEM} 
          })
          // console.log(responseTms)
          response[0].CARGO_DEPARTURE_FROM_PNP_DATE_IN = responseTms.data && responseTms.data[0].atd ? moment(responseTms.data[0].atd.date) : null
          response[0].CARGO_DEPARTURE_FROM_PNP_TIME_OUT = responseTms.data && responseTms.data[0].actual_time_out ? moment(responseTms.data[0].actual_time_out.date).format("HH:mm") : null
          response[0].CARGO_ARRIVAL_TO_PNP_TIME_IN = responseTms.data && responseTms.data[0].actual_time_in ? moment(responseTms.data[0].actual_time_in.date).format("HH:mm") : null
        } catch (e) {
          console.log(e)
        }
      }

      return response[0] ? response[0] : null
    },
    getRouteTruckDomestic: async (do_no) => {
      const sql = `
      SELECT 
        dhy.ETA,
        vt.actual_eta_date,
        vt.actual_eta_time,
        dhy.plan_gl_ship_date
      FROM ots_document_hy dhy
      LEFT JOIN v_spt_truck vt ON vt.pi_no = dhy.do
      where dhy.do = '${do_no}'`
      return await query(sql)
    }
  }
}