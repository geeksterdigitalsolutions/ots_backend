const oracleSql = require('./oracle.sql')
const moment = require('moment')

const countSummarySql = (begin, end, customer_code) => `
      SELECT 
        COUNT(*) AS total_shipment,
        SUM (
            CASE f.shipment_status
              WHEN 'completed' THEN 1
              ELSE 0
            END
        ) AS COMPLETED,
        SUM (
            CASE f.shipment_status
              WHEN 'completed' THEN 0
              ELSE 1
            END
        ) AS PENDING,
        SUM (
            CASE f.delivery_on_time
              WHEN 'ON TIME' THEN 1
              ELSE 0
            END
        ) AS ON_TIME,
        SUM (
            CASE f.delivery_on_time
              WHEN 'DELAY 4-10 DAYS' THEN 1
              ELSE 0
            END
        ) AS DELAY_4_10_DAYS,
        SUM (
            CASE f.delivery_on_time
              WHEN 'MORE THAN 10 DAYS DELAY' THEN 1
              ELSE 0
            END
        ) AS DELEY_OVER_THAN_11_DAYS
      FROM v_spt_freight v
      LEFT JOIN ots_frieght f ON v.booking_no = f.booking_no and v.delivery_no = f.delivery_no
      WHERE v.eta BETWEEN TO_DATE('${begin}', 'YYYY-MM-DD') AND TO_DATE('${end}', 'YYYY-MM-DD') AND v.customer_code = '${customer_code}'`

module.exports = (oracleConnection) => {
  const { query, upsert, pagination, parseFilter } = oracleSql(oracleConnection)
  return {
    getFrieghtAdminList: async (options = {}) => {
      const sort = {
        name: options.sort_name || 'ETD_PORT',
        by: options.sort_by || 'DESC'
      }

      const filters = {}
      if (options.CI_NO && options.CI_NO != '') {
        filters['v.CI_NO'] = options.CI_NO
      }
      if (options.BOOKING_NO && options.BOOKING_NO != '') {
        filters['v.BOOKING_NO'] = options.BOOKING_NO
      }
      if (options.SO_NO && options.SO_NO != '') {
        filters['v.SO_NO'] = options.SO_NO
      }
      if (options.DELIVERY_NO && options.DELIVERY_NO != '') {
        filters['v.DELIVERY_NO'] = options.DELIVERY_NO
      }
      if (options.DESTINATION_PORT && options.DESTINATION_PORT != '') {
        filters['v.DESTINATION_PORT'] = options.DESTINATION_PORT
      }
      if (options.INVOICE && options.INVOICE != '') {
        filters['f.INVOICE'] = options.INVOICE
      }
      if (options.SHIPMENT_STATUS && options.SHIPMENT_STATUS != '') {
        filters['f.SHIPMENT_STATUS'] = options.SHIPMENT_STATUS
      }
      if (options.ETD_PORT_OF_TRANSHIPMENT) {
        filters['f.ETD_PORT_OF_TRANSHIPMENT'] = options.ETD_PORT_OF_TRANSHIPMENT
      }
      if (options.ETA_PORT_OF_DESTINATION) {
        filters['v.ETA'] = options.ETA_PORT_OF_DESTINATION
      }
      if (options.ETD_PORT) {
        filters['v.ETD_PORT'] = options.ETD_PORT
      }
      if (options.RESPONSIBILITY && options.RESPONSIBILITY != '') {
        filters['f.RESPONSIBILITY'] = options.RESPONSIBILITY
      }
      if (options.DOCUMENT_STATUS && options.DOCUMENT_STATUS != '') {
        filters['d.status_of_document'] = options.DOCUMENT_STATUS
      }
      if (options.CUSTOMER_NAME && options.CUSTOMER_NAME != '') {
        filters['v.CUSTOMER_NAME'] = options.CUSTOMER_NAME
      }

      if (Object.keys(filters).length == 0) {
        filters['v.ETD_PORT'] = {
          from: moment().subtract(1, 'M').startOf('month').format('YYYY-MM-DD'),
          to: moment().add(1, 'M').endOf('month').format('YYYY-MM-DD'),
        }
      }

      const filtersCommand = parseFilter(filters)
      const sql = `
        SELECT
          v.*,
          m.status,
          f.month,
          f.fwd,
          f.etd_port_of_loading,
          f.actual_etd_port_of_loading,
          f.port_of_transhipment,
          f.etd_port_of_transhipment,
          f.eta_port_of_transhipment,
          f.mother_vessel_name,
          v.eta as eta_port_of_destination,
          f.on_time,
          f.delivery_on_time,
          f.frieght_usd,
          f.vgm,
          f.invoice,
          "DATE",
          f.due,
          f.frieght_url,
          f.shipment_status,
          f.actual_etd,
          f.responsibility,
          f.actual_eta,
          f.no_postpone,
          f.remark,
          f.reason,
          f.baht,
          f.myr,
          f.place_bkg_date,
          f.release_bkg_date,
          f.reason_booking,
          f.freetime,
          f.reference,
          d.status_of_document as DOCUMENT_STATUS,
          row_number() over (ORDER BY v.${sort.name} ${sort.by}) line_number
        FROM v_spt_freight v
        LEFT JOIN ots_master_order m ON v.booking_no = m.booking_no AND v.delivery_no = m.delivery_no
        LEFT JOIN ots_frieght f ON v.booking_no = f.booking_no and v.delivery_no = f.delivery_no
        LEFT JOIN ots_document d ON v.booking_no = d.booking_no and v.delivery_no = d.delivery_no ${filtersCommand}`
      
      return await pagination(sql, options)
    },
    getFrightDetail: async (booking_no, delivery_no) => {
      const sql = `SELECT
        v.*,
        d.bl_no,
        m.status,
        f.month,
        f.fwd,
        f.etd_port_of_loading,
        f.actual_etd_port_of_loading,
        f.port_of_transhipment,
        f.etd_port_of_transhipment,
        f.eta_port_of_transhipment,
        f.mother_vessel_name,
        v.eta as eta_port_of_destination,
        f.on_time,
        f.delivery_on_time,
        f.frieght_usd,
        f.vgm,
        f.invoice,
        "DATE",
        f.due,
        f.frieght_url,
        f.shipment_status,
        f.actual_etd,
        f.responsibility,
        f.actual_eta,
        f.no_postpone,
        f.remark,
        f.reason,
        f.baht,
        f.myr,
        f.place_bkg_date,
        f.release_bkg_date,
        f.reason_booking,
        f.freetime,
        f.reference,
        d.status_of_document as DOCUMENT_STATUS
      FROM v_spt_freight v
      LEFT JOIN ots_master_order m ON v.booking_no = m.booking_no AND v.delivery_no = m.delivery_no
      LEFT JOIN ots_frieght f ON v.booking_no = f.booking_no and v.delivery_no = f.delivery_no
      LEFT JOIN ots_document d ON v.booking_no = d.booking_no AND v.delivery_no = d.delivery_no
      WHERE v.BOOKING_NO = '${booking_no}' AND v.DELIVERY_NO = '${delivery_no}'`

      return await query(sql)
    },
    checkShipmentStatusComplete: async (booking_no, delivery_no) => {
      const sql = `SELECT shipment_status FROM ots_frieght WHERE booking_no = '${booking_no}' AND delivery_no = '${delivery_no}'`
      const frieght = await query(sql)
      return frieght.length > 0 && frieght[0]['SHIPMENT_STATUS'] == 'completed'
    },
    findViewFrieghtByBookingNo: async (booking_no) => {
      return await query(`SELECT * FROM v_spt_freight WHERE BOOKING_NO = '${booking_no}'`)
    },
    upsertMasterOrder: async (primary, data) => {
      return await upsert('ots_master_order', primary, data)
    },
    upsertFrieght: async (primary, data) => {
      return upsert('ots_frieght', primary, data)
    },
    isHasFrieght: async (booking_no) => {
      const countSql = `SELECT count(v.booking_no) as "count" FROM v_spt_freight v WHERE booking_no = '${booking_no}'`
      const total = await query(countSql)
      return total[0] && total[0].count > 0
    },
    getDestinations: async () => {
      const sqlCommand = `SELECT * FROM SO_PORT WHERE ACTIVE = 'Y'`
      return query(sqlCommand)
    },
    getSummaryFrieghtStatus: async (options) => {
      
      const lastMonthSql = countSummarySql(moment().subtract(1, 'M').startOf('month').format('YYYY-MM-DD'), moment().subtract(1, 'M').endOf('month').format('YYYY-MM-DD'), options.CUSTOMER_CODE)
      const currentMonthSql = countSummarySql(moment().startOf('month').format('YYYY-MM-DD'), moment().endOf('month').format('YYYY-MM-DD'), options.CUSTOMER_CODE)
      const nextMonthSql = countSummarySql(moment().add(1, 'M').startOf('month').format('YYYY-MM-DD'), moment().add(1, 'M').endOf('month').format('YYYY-MM-DD'), options.CUSTOMER_CODE)
      
      const response = await Promise.all([query(lastMonthSql), query(currentMonthSql), query(nextMonthSql)])
      // return response
      return response.map((res, i) => {
        const item = {}
        for (let field in res[0]) {
          item[field] = res[0][field] != null ? res[0][field] : 0
        }
        return { ...item, PERIOD: moment().subtract(1, 'M').add(i, 'M').format('MMM-YY') }
      })
    },
    getFrieghtAdminDashboard: async() => {
      const previousMonth = moment().subtract(1, 'M').startOf('month').format('YYYY-MM-DD')
      const nextMonth = moment().add(1, 'M').endOf('month').format('YYYY-MM-DD')
      const sql = `SELECT 
          COUNT(*) AS total_shipment,
          SUM (
            CASE f.shipment_status
              WHEN 'completed' THEN 1
              ELSE 0
            END
          ) AS SHIPMENT_COMPLETED,
          SUM (
            CASE WHEN (f.shipment_status = 'pending' OR f.shipment_status IS NULL) THEN 1
            ELSE 1
            END
          ) AS SHIPMENT_PENDING,
          SUM (
            CASE d.status_of_document
              WHEN 'completed' THEN 1
              ELSE 0
            END
          ) AS DOCUMENT_COMPLETED,
          SUM (
            CASE WHEN (d.status_of_document = 'pending' OR d.status_of_document IS NULL) THEN 1
            ELSE 0
            END
          ) AS DOCUMENT_PENDING,
          SUM (
            CASE WHEN f.shipment_status = 'completed' AND v.CTN_SIZE NOT LIKE 'Break Bulk' THEN v.VOL
            ELSE 0
            END
          ) AS TOTAL_CONTAINER_COMPLETED,
          SUM (
            CASE WHEN (f.shipment_status = 'pending' OR f.shipment_status IS NULL) AND v.CTN_SIZE NOT LIKE 'Break Bulk' THEN v.VOL
            ELSE 0
            END
          ) AS TOTAL_CONTAINER_PENDING,
          SUM (
            CASE WHEN f.shipment_status = 'completed' AND v.CTN_SIZE LIKE 'Break Bulk' THEN v.VOL
            ELSE 0
            END
          ) AS TOTAL_BULK_COMPLETED,
          SUM (
            CASE WHEN (f.shipment_status = 'pending' OR f.shipment_status IS NULL) AND v.CTN_SIZE LIKE 'Break Bulk' THEN v.VOL
            ELSE 0
            END
          ) AS TOTAL_BULK_PENDING
        FROM v_spt_freight v
        LEFT JOIN ots_frieght f ON v.booking_no = f.booking_no and v.delivery_no = f.delivery_no
        LEFT JOIN ots_document d ON v.booking_no = d.booking_no and v.delivery_no = d.delivery_no
        WHERE v.eta BETWEEN TO_DATE('${previousMonth}', 'YYYY-MM-DD') AND TO_DATE('${nextMonth}', 'YYYY-MM-DD')
      `
      return query(sql)
    }
  }
}