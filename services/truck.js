
const oracleSql = require('./oracle.sql')
const moment = require('moment')

const countSummarySql = (begin, end, customer_code) => `
  SELECT 
    COUNT(*) AS total_shipment,
    SUM (
      CASE WHEN t.estimate_arrival_date IS NOT NULL THEN 1 END
    ) AS COMPLETED,
    SUM (
      CASE WHEN t.estimate_arrival_date IS NULL THEN 1 END
    ) AS PENDING,
    0 AS ON_TIME,
    0 AS DELAY_4_10_DAYS,
    0 AS DELEY_OVER_THAN_11_DAYS
  FROM ots_truck t
  JOIN v_spt_truck_tms vt ON t.do = vt.pi_no AND t.invoice_no = vt.ci_no
  WHERE vt.eta_port BETWEEN TO_DATE('${begin}', 'YYYY-MM-DD') AND TO_DATE('${end}', 'YYYY-MM-DD') AND vt.customer_code = '${customer_code}'`

module.exports = (oracleConnection) => {
  const { query, upsert, pagination, parseFilter } = oracleSql(oracleConnection)

  return {
    getTruckList: async (options) => {
      const sort = {
        name: options.sort_name || 'ETD_PORT',
        by: options.sort_by || 'DESC'
      }

      const filters = {}

      if (options.CI_NO && options.CI_NO != '') {
        filters['CI_NO'] = options.CI_NO
      }

      if (options.PI_NO && options.PI_NO != '') {
        filters['PI_NO'] = options.PI_NO
      }

      if (options.PO_NO && options.PO_NO != '') {
        filters['PO_NO'] = options.PO_NO
      }

      if (options.SO_NO_REF && options.SO_NO_REF != '') {
        filters['SO_NO_REF'] = options.SO_NO_REF
      }

      if (Object.keys(filters).length == 0) {
        filters['ETD_PORT'] = {
          from: moment().subtract(1, 'M').startOf('month').format('YYYY-MM-DD'),
          to: moment().add(1, 'M').endOf('month').format('YYYY-MM-DD'),
        }
      }

      const filtersCommand = parseFilter(filters)

      const sql = `SELECT 
      vt.*,
      t.cargo_border_to_bkhwh_date,
      t.cargo_border_to_bkhwh_time,
      t.my_driver,
      t.my_truck_no,
      t.d_o_no,
      t.cargo_bkhwh_to_customer_date,
      t.cargo_bkhwh_to_customer_time,
      t.consignee_name,
      t.location,
      t.estimate_arrival_date,
      t.estimate_arrival_time,
      t.cargo_pnp_to_cross_border_date,
      t.cargo_pnp_to_cross_border_time,
      t.shipping_my,
      t.transportation_on_name,
      t.warehouse_name,
      t.invoice_no,
      d.tracking_no,
      d.courier_by,
      row_number() over (ORDER BY vt.${sort.name} ${sort.by}) line_number
      FROM (
        SELECT 
          ci_no,
          so_no_ref,
          pi_no,
          po_no,
          customer_code,
          sum(gross_weight) as gross_weight,
          sum(pallet_qty) as pallet_qty,
          min(payment_status) as payment_status,
          min(ar_name) as ar_name,
          min(so_item) as so_item,
          min(port_desc) as port_desc,
          min(etd_port) as etd_port,
          min(sub_trans_type) as sub_trans_type
        FROM v_spt_truck_tms ${filtersCommand} GROUP BY ci_no, so_no_ref, pi_no, po_no, customer_code
      ) vt
      LEFT JOIN ots_truck t ON vt.pi_no = t.do AND vt.ci_no = t.invoice_no
      LEFT JOIN ots_document d ON vt.pi_no = d.delivery_no`
      
      return await pagination(sql, options)
    },
    getTruckDetail: async (so, ci_no, pi_no, po_no, customer_code, item) => {
      const sql = `SELECT 
        v.*,
        t.cargo_border_to_bkhwh_date,
        t.cargo_border_to_bkhwh_time,
        t.my_driver,
        t.my_truck_no,
        t.d_o_no,
        t.cargo_bkhwh_to_customer_date,
        t.cargo_bkhwh_to_customer_time,
        t.consignee_name,
        t.location,
        t.estimate_arrival_date,
        t.estimate_arrival_time,
        t.cargo_pnp_to_cross_border_date,
        t.cargo_pnp_to_cross_border_time,
        t.shipping_my,
        t.transportation_on_name,
        t.warehouse_name
      from v_spt_truck_tms v
      LEFT JOIN ots_truck t ON v.pi_no = t.do AND v.ci_no = t.invoice_no
      WHERE v.SO_NO_REF = '${so}' AND v.CI_NO = '${ci_no}' AND v.PI_NO = '${pi_no}' AND v.PO_NO = '${po_no}' AND v.CUSTOMER_CODE = '${customer_code}' AND v.SO_ITEM = '${item}'`

      return await query(sql)
    },
    countOfTruck: async () => {
      const countSql = `SELECT count(*) as "count" FROM ots_truck`
      const count = await query(countSql)
      return count[0] ? count[0].count : 0
    },
    upsertTruck: async (primary, data) => {
      return await upsert('ots_truck', primary, data)
    },
    getTruckViewList: async (options) => {

      const filters = {}
      
      if (options.PI_NO && options.PI_NO != '') {
        filters['PI_NO'] = options.PI_NO
      }
      if (options.CI_NO && options.CI_NO != '') {
        filters['CI_NO'] = options.CI_NO
      }
      if (options.PO_NO && options.PO_NO != '') {
        filters['PO_NO'] = options.PO_NO
      }
      if (options.CUSTOMER_CODE && options.CUSTOMER_CODE != '') {
        filters['CUSTOMER_CODE'] = options.CUSTOMER_CODE
      }

      if (options.SO_NO_REF && options.SO_NO_REF != '') {
        filters['SO_NO_REF'] = options.SO_NO_REF
      }

      const filtersCommand = parseFilter(filters)

      const sql = `
      SELECT 
        vt.pi_no,
        vt.so_no_ref,
        vt.ci_no,
        vt.po_no,
        vt.so_item,
        vt.customer_code,
        vt.port_desc as destination_port,
        vt.etd_port as etd,
        vt.gross_weight,
        vt.pallet_qty,
        vt.eta_port,
        vf.vol,
        vt.payment_status,
        dhy.status as document_status,
        dhy.eta,
        dhy.so,
        f.shipment_status,
        f.actual_eta,
        t.estimate_arrival_date,
        row_number() over (ORDER BY vt.etd_port desc) line_number
        FROM (
          SELECT 
            ci_no,
            so_no_ref,
            pi_no,
            po_no,
            customer_code,
            min(payment_status) as payment_status,
            sum(gross_weight) as gross_weight,
            sum(pallet_qty) as pallet_qty,
            min(so_item) as so_item,
            min(port_desc) as port_desc,
            min(etd_port) as etd_port,
            min(eta_port) as eta_port,
            min(sub_trans_type) as sub_trans_type
          FROM v_spt_truck_tms ${filtersCommand} GROUP BY ci_no, so_no_ref, pi_no, po_no, customer_code
        ) vt
        LEFT JOIN v_spt_freight vf ON vt.ci_no = vf.ci_no AND vt.pi_no = vf.delivery_no
        LEFT JOIN ots_document_hy dhy ON dhy.do = vt.pi_no AND dhy.COMMERCIAL_INVOICE = vt.ci_no
        LEFT JOIN ots_frieght f ON f.DELIVERY_NO = vt.pi_no
        LEFT JOIN ots_truck t ON vt.ci_no = t.invoice_no AND vt.pi_no = t.do`

      return await pagination(sql, options)
    },
    getSummaryTruckStatus: async (options) => {
      
      const lastMonthSql = countSummarySql(moment().subtract(1, 'M').startOf('month').format('YYYY-MM-DD'), moment().subtract(1, 'M').endOf('month').format('YYYY-MM-DD'), options.CUSTOMER_CODE)
      const currentMonthSql = countSummarySql(moment().startOf('month').format('YYYY-MM-DD'), moment().endOf('month').format('YYYY-MM-DD'), options.CUSTOMER_CODE)
      const nextMonthSql = countSummarySql(moment().add(1, 'M').startOf('month').format('YYYY-MM-DD'), moment().add(1, 'M').endOf('month').format('YYYY-MM-DD'), options.CUSTOMER_CODE)
    
      const response = await Promise.all([query(lastMonthSql), query(currentMonthSql), query(nextMonthSql)])
      // return response
      return response.map((res, i) => {
        const item = {}
        for (let field in res[0]) {
          item[field] = res[0][field] != null ? res[0][field] : 0
        }
        return { ...item, PERIOD: moment().subtract(1, 'M').add(i, 'M').format('MMM-YY') }
      })
    },
    getAdminTruckDashboard: async (options) => {
      const sql = `SELECT
      SUM (
        CASE
          WHEN estimate_arrival_date IS NOT NULL THEN 1
          ELSE 0
        END
      ) AS SHIPMENT_COMPLETED,
      SUM (
        CASE
          WHEN estimate_arrival_date IS NULL THEN 1
          ELSE 0
        END
      ) AS SHIPMENT_PENDING
      FROM ots_truck`
      return query(sql)
    }
  }
}