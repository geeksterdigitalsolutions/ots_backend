
const oracleSql = require('./oracle.sql')
const moment = require('moment')
const axios = require('axios')

module.exports = (oracleConnection) => {
  const { query, upsert, pagination, parseFilter } = oracleSql(oracleConnection)

  const authCredential = {
    username: process.env.GLOBAL_PI_USERNAME,
    password: process.env.GLOBAL_PI_PASSWORD
  }

  const sortByEtd = (a, b) => {
    if (moment(a.invCrDate).isSameOrAfter(b.invCrDate)) {
      return -1;
    }
    return 1;
  }

  return {
    getTruckDomesticList: async (options) => {
      const page = options.page || 1
      const length = options.length || 10
      try {
        const filters = {
          PLAN_GI_DATE: {
            from: options.PLAN_GI_DATE_FROM || moment().subtract(1, 'M').startOf('month').format('YYYYMMDD'),
            to: options.PLAN_GI_DATE_TO || moment().add(1, 'M').endOf('month').format('YYYYMMDD')
          }
        }
        const params = {
          deliveryDate: {
            item: [
              {
                sign: "I",
                option: "BT",
                low: filters.PLAN_GI_DATE.from,
                high: filters.PLAN_GI_DATE.to
              }
            ]
          },
          vtweg: {
            item: [
              {
                sign: "I",
                option: "EQ",
                low: "10"
              }
            ]
          }
        }
        if (options.DO) {
          params.vbeln = {
            item: [
              {
                sign: "I",
                option: "EQ",
                low: options.DO
              }
            ]
          }
        }
        const response = await axios.post(process.env.GLOBAL_PI_URL,
        params,
        {
          auth: authCredential,
          headers: {
            'Content-Type': 'application/json'
          }
        })
  
        const { delivery } =response.data
  
        if (!delivery || !delivery.item) return {
          items: []
        }
        if (options.CUSTOMER_CODE) {
          delivery.item = delivery.item.filter(item => parseInt(item.custCode) === parseInt(options.CUSTOMER_CODE))
        }
        if (options.PI_NO) {
          delivery.item = delivery.item.filter(item => item.orderNo == parseInt(options.PI_NO))
        }
        if (options.CARGO_DEPARTURE_FROM_PNP_DATE_FROM) {
          delivery.item = delivery.item.filter(item => moment(item.invCrDate).isSameOrAfter(options.CARGO_DEPARTURE_FROM_PNP_DATE_FROM))
        }
        if (options.CARGO_DEPARTURE_FROM_PNP_DATE_TO) {
          delivery.item = delivery.item.filter(item => moment(item.invCrDate).isSameOrBefore(options.CARGO_DEPARTURE_FROM_PNP_DATE_TO))
        }
        return {
          page,
          length,
          total_count: delivery.item.length,
          last_page: Math.ceil(delivery.item.length / length),
          items: delivery.item.sort(sortByEtd).splice((page-1)*length, length).map((td) => ({
            SO_ITEM: parseInt(td.delvItem),
            DO: td.delvNo,
            PI_NO: td.orderNo,
            PLANT: td.plant,
            PO_NO: td.po,
            CUSTOMER_NAME: td.custName,
            QTY_OF_PALLET: td.delvQty,
            GROSS_WEIGHT: td.convQty,
            CARGO_DEPARTURE_FROM_PNP_DATE: td.invCrDate,
          }))
        }
      } catch (e) {
        console.log(e)
        return { items: [] }
      }
    },
    getTruckDomesticExport: async (truckDomesticIncludes, truckDomesticExcludes, options) => {
      try {
        const filters = {
          PLAN_GI_DATE: {
            from: options.PLAN_GI_DATE_FROM || moment().subtract(1, 'M').startOf('month').format('YYYYMMDD'),
            to: options.PLAN_GI_DATE_TO || moment().add(1, 'M').endOf('month').format('YYYYMMDD')
          }
        }
        const params = {
          deliveryDate: {
            item: [
              {
                sign: "I",
                option: "BT",
                low: filters.PLAN_GI_DATE.from,
                high: filters.PLAN_GI_DATE.to
              }
            ]
          },
          vtweg: {
            item: [
              {
                sign: "I",
                option: "EQ",
                low: "10"
              }
            ]
          }
        }
        if (options.DO) {
          params.vbeln = {
            item: [
              {
                sign: "I",
                option: "EQ",
                low: options.DO
              }
            ]
          }
        }
        const response = await axios.post(process.env.GLOBAL_PI_URL,
        params,
        {
          auth: authCredential,
          headers: {
            'Content-Type': 'application/json'
          }
        })
  
        const { delivery } =response.data
  
        if (!delivery || !delivery.item) return []

        let items = []
        if (truckDomesticIncludes.length > 0) {
          items = delivery.item.filter(td => truckDomesticIncludes.some(include => (td.delvNo === include.DO && parseInt(td.delvItem) === include.ITEM)))
        } else if (truckDomesticExcludes.length > 0) {
          items = delivery.item.filter(td => !truckDomesticExcludes.some(exclude => (td.delvNo === exclude.DO && parseInt(td.delvItem) === exclude.ITEM)))
        } else {
          items = delivery.item
        }

        if (options.PI_NO) {
          items = items.filter(item => item.orderNo == parseInt(options.PI_NO))
        }
        if (options.CARGO_DEPARTURE_FROM_PNP_DATE_FROM) {
          items = items.filter(item => moment(item.invCrDate).isSameOrAfter(options.CARGO_DEPARTURE_FROM_PNP_DATE_FROM))
        }
        if (options.CARGO_DEPARTURE_FROM_PNP_DATE_TO) {
          items = items.filter(item => moment(item.invCrDate).isSameOrBefore(options.CARGO_DEPARTURE_FROM_PNP_DATE_TO))
        }

        items = items.sort(sortByEtd)

        return Promise.all(items.map(async (td) => {
          const response = await query(`SELECT * FROM ots_truck_domestic WHERE DO_NO = '${td.delvNo}' AND ITEM = '${td.delvItem}'`)
          const TRUCK_TYPE = response[0] ? response[0].TRUCK_TYPE : null
          const TRANSPORTATION_NAME = response[0] ? response[0].TRANSPORTATION_NAME : null
          const WAREHOUSE_NAME = response[0] ? response[0].WAREHOUSE_NAME : td.plant
          const LOCATION = response[0] ? response[0].LOCATION : td.country
          const ESTIMATE_ARRIVAL_DATE = response[0] ? response[0].ESTIMATE_ARRIVAL_DATE : null
          const ESTIMATE_ARRIVAL_TIME = response[0] ? response[0].ESTIMATE_ARRIVAL_TIME : null
          const PHONE = response[0] ? response[0].PHONE : null
          return {
            SO_ITEM: parseInt(td.delvItem),
            DO: td.delvNo,
            PI_NO: td.orderNo,
            PLANT: td.plant,
            PO_NO: td.po,
            CUSTOMER_NAME: td.custName,
            QTY_OF_PALLET: td.delvQty,
            GROSS_WEIGHT: td.convQty,
            CARGO_DEPARTURE_FROM_PNP_DATE: moment(td.invCrDate, 'YYYY-MM-DD'),
            CARGO_DEPARTURE_FROM_PNP_TIME: td.invCrTime.substring(0,5),
            ITEM_DESC: td.matDesc,
            TRUCK_TYPE,
            TRANSPORTATION_NAME,
            WAREHOUSE_NAME,
            LOCATION,
            ESTIMATE_ARRIVAL_DATE,
            ESTIMATE_ARRIVAL_TIME,
            PHONE,
            STATUS: ESTIMATE_ARRIVAL_DATE ? 'completed' : 'pending'
          }
        }))
      } catch (e) {
        console.log(e)
        return []
      }
    },
    getTruckDomesticDetail: async (deliveryNo, deliveryItem, options) => {
      try {
        const filters = {
          PLAN_GI_DATE: {
            from: options.PLAN_GI_DATE_FROM || moment().subtract(1, 'M').startOf('month').format('YYYYMMDD'),
            to: options.PLAN_GI_DATE_TO || moment().add(1, 'M').endOf('month').format('YYYYMMDD')
          }
        }
        const response = await axios.post(process.env.GLOBAL_PI_URL,
        {
          deliveryDate: {
            item: [
              {
                sign: "I",
                option: "BT",
                low: filters.PLAN_GI_DATE.from,
                high: filters.PLAN_GI_DATE.to
              }
            ]
          },
          vbeln: {
            item: [
              {
                sign: "I",
                option: "EQ",
                low: deliveryNo
              }
            ]
          },
          vtweg: {
            item: [
              {
                sign: "I",
                option: "EQ",
                low: "10"
              }
            ]
          }
        },
        {
          auth: authCredential,
          headers: {
            'Content-Type': 'application/json'
          }
        })
  
        const { delivery } =response.data
  
        if (!delivery || !delivery.item) return []

        const items = delivery.item.filter(td => td.delvItem === `${deliveryItem}`.padStart(6, '0'))

        return Promise.all(items.map(async (td) => {
          const response = await query(`SELECT * FROM ots_truck_domestic WHERE DO_NO = '${deliveryNo}' AND ITEM = '${deliveryItem}'`)
          const TRUCK_TYPE = response[0] ? response[0].TRUCK_TYPE : null
          const TRANSPORTATION_NAME = response[0] ? response[0].TRANSPORTATION_NAME : null
          const WAREHOUSE_NAME = response[0] ? response[0].WAREHOUSE_NAME : td.plant
          const LOCATION = response[0] ? response[0].LOCATION : td.country
          const ESTIMATE_ARRIVAL_DATE = response[0] ? response[0].ESTIMATE_ARRIVAL_DATE : null
          const ESTIMATE_ARRIVAL_TIME = response[0] ? response[0].ESTIMATE_ARRIVAL_TIME : null
          const PHONE = response[0] ? response[0].PHONE : null
          return {
            SO_ITEM: parseInt(td.delvItem),
            DO: td.delvNo,
            PI_NO: td.orderNo,
            PLANT: td.plant,
            PO_NO: td.po,
            CUSTOMER_NAME: td.custName,
            QTY_OF_PALLET: td.delvQty,
            GROSS_WEIGHT: td.convQty,
            ITEM_DESC: td.matDesc,
            CARGO_DEPARTURE_FROM_PNP_DATE: moment(td.invCrDate, 'YYYY-MM-DD'),
            CARGO_DEPARTURE_FROM_PNP_TIME: td.invCrTime.substring(0,5),
            TRUCK_TYPE,
            TRANSPORTATION_NAME,
            WAREHOUSE_NAME,
            LOCATION,
            ESTIMATE_ARRIVAL_DATE,
            ESTIMATE_ARRIVAL_TIME,
            PHONE,
            STATUS: ESTIMATE_ARRIVAL_DATE ? 'completed' : 'pending'
          }
        }))
      } catch (e) {
        console.log(e)
        return []
      }
    },
    upsertTruckDomestic: async (primary, data) => {
      return await upsert('ots_truck_domestic', primary, data)
    },
    getTruckDomesticViewList: async (options) => {
      const page = options.page || 1
      const length = options.length || 10
      try {
        const filters = {
          PLAN_GI_DATE: {
            from: options.PLAN_GI_DATE_FROM || moment().subtract(1, 'M').startOf('month').format('YYYYMMDD'),
            to: options.PLAN_GI_DATE_TO || moment().add(1, 'M').endOf('month').format('YYYYMMDD')
          }
        }
        const params = {
          deliveryDate: {
            item: [
              {
                sign: "I",
                option: "BT",
                low: filters.PLAN_GI_DATE.from,
                high: filters.PLAN_GI_DATE.to
              }
            ]
          },
          vtweg: {
            item: [
              {
                sign: "I",
                option: "EQ",
                low: "10"
              }
            ]
          }
        }
        if (options.DO) {
          params.vbeln = {
            item: [
              {
                sign: "I",
                option: "EQ",
                low: options.DO
              }
            ]
          }
        }
        const response = await axios.post(process.env.GLOBAL_PI_URL,
        params,
        {
          auth: authCredential,
          headers: {
            'Content-Type': 'application/json'
          }
        })
  
        const { delivery } = response.data
  
        if (!delivery || !delivery.item) return {
          items: []
        }
        if (options.CUSTOMER_CODE) {
          delivery.item = delivery.item.filter(item => parseInt(item.custCode) === parseInt(options.CUSTOMER_CODE))
        }
        if (options.PI_NO) {
          delivery.item = delivery.item.filter(item => item.orderNo == parseInt(options.PI_NO))
        }
        if (options.CARGO_DEPARTURE_FROM_PNP_DATE_FROM) {
          delivery.item = delivery.item.filter(item => moment(item.invCrDate).isSameOrAfter(options.CARGO_DEPARTURE_FROM_PNP_DATE_FROM))
        }
        if (options.CARGO_DEPARTURE_FROM_PNP_DATE_TO) {
          delivery.item = delivery.item.filter(item => moment(item.invCrDate).isSameOrBefore(options.CARGO_DEPARTURE_FROM_PNP_DATE_TO))
        }

        let items = delivery.item.sort(sortByEtd).splice((page-1)*length, length)

        items = await Promise.all(items.map(async (td) => {
          const response = await query(`SELECT * FROM ots_truck_domestic WHERE DO_NO = '${td.delvNo}' AND ITEM = '${td.delvItem}'`)
          const TRUCK_TYPE = response[0] ? response[0].TRUCK_TYPE : null
          const TRANSPORTATION_NAME = response[0] ? response[0].TRANSPORTATION_NAME : null
          const WAREHOUSE_NAME = response[0] ? response[0].WAREHOUSE_NAME : td.plant
          const LOCATION = response[0] ? response[0].LOCATION : td.country
          const ESTIMATE_ARRIVAL_DATE = response[0] ? response[0].ESTIMATE_ARRIVAL_DATE : null
          const ESTIMATE_ARRIVAL_TIME = response[0] ? response[0].ESTIMATE_ARRIVAL_TIME : null
          const PHONE = response[0] ? response[0].PHONE : null
          return {
            SO_ITEM: parseInt(td.delvItem),
            DO: td.delvNo,
            PI_NO: td.orderNo,
            PLANT: td.plant,
            PO_NO: td.po,
            CUSTOMER_NAME: td.custName,
            QTY_OF_PALLET: td.delvQty,
            GROSS_WEIGHT: td.convQty,
            ITEM_DESC: td.matDesc,
            CARGO_DEPARTURE_FROM_PNP_DATE: moment(td.invCrDate, 'YYYY-MM-DD'),
            CARGO_DEPARTURE_FROM_PNP_TIME: td.invCrTime.substring(0,5),
            TRUCK_TYPE,
            TRANSPORTATION_NAME,
            WAREHOUSE_NAME,
            LOCATION,
            ESTIMATE_ARRIVAL_DATE,
            ESTIMATE_ARRIVAL_TIME,
            PHONE,
            STATUS: ESTIMATE_ARRIVAL_DATE ? 'completed' : 'pending'
          }
        }))

        return {
          page,
          length,
          total_count: delivery.item.length,
          last_page: Math.ceil(delivery.item.length / length),
          items
        }
      } catch (e) {
        console.log(e)
        return { items: [] }
      }
    },
  }
}