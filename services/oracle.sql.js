'use strict'

module.exports = (oracleConnection) => {
  const query = async (sql) => {
    let conn
    try {
      conn = await oracleConnection.getConnection()
      // console.log(sql)
      const result = await conn.execute(sql)
      const { metaData, rows } = result
      return rows.map(row => {
        const item = {}
        metaData.forEach((field, index) => {
          item[field.name] = row[index]
        })
        return item
      })
    } finally {
      if (conn) {
        conn.close().catch((err) => {
          console.log(err)
        })
        console.log('conn has close')
      }
    }
  }

  return {
    query,
    upsert: async (table, primary, data) => {
      const insertData = {...primary, ...data}

      const insert = `INSERT INTO ${table} VALUES (${Object.keys(insertData).map(d => `:${d}`).join(', ')})`
      const update = `UPDATE ${table} SET ${Object.keys(data).map(d => `${d} = :${d}`).join(', ')} WHERE ${Object.keys(primary).map(p => `${p} = :${p}`).join(' AND ')}`
      var conn = await oracleConnection.getConnection()
      var mapData = {}
      for (let i in insertData) {
        mapData[i] = { val: insertData[i] }
      }
      try {
        const result = await conn.execute(insert, mapData, { autoCommit: true })
        return result
      } catch(e) {
        if (e.errorNum === 1) {
          await conn.execute(update, mapData, { autoCommit: true })
        } else {
          throw e
        }
      } finally {
        if (conn) {
          conn.close().catch((err) => {})
          console.log('conn has close')
        }
      }
    },
    insert: async (table, data) => {
      const insert = `INSERT INTO ${table} VALUES (${Object.keys(data).map(d => `:${d}`).join(', ')})`
      var conn = await oracleConnection.getConnection()
      var mapData = {}
      for (let i in data) {
        mapData[i] = { val: data[i] }
      }
      try {
        const result = await conn.execute(insert, mapData, { autoCommit: true })
        return result
      } catch(e) {
        throw e
      } finally {
        if (conn) {
          conn.close().catch((err) => {})
          console.log('conn has close')
        }
      }
    },
    update: async (table, primary, data) => {
      const update = `UPDATE ${table} SET ${Object.keys(data).map(d => `${d} = :${d}`).join(', ')} WHERE ${Object.keys(primary).map(p => `${p} = :${p}`).join(' AND ')}`
      var conn = await oracleConnection.getConnection()
      var mapData = {}
      data = Object.assign(primary, data)
      for (let i in data) {
        mapData[i] = { val: data[i] }
      }
      try {
        const result = await conn.execute(update, mapData, { autoCommit: true })
        return result
      } catch(e) {
        throw e
      } finally {
        if (conn) {
          conn.close().catch((err) => {})
          console.log('conn has close')
        }
      }
    },
    remove: async (table, primary) => {
      const sql = `DELETE FROM ${table} WHERE ${Object.keys(primary).map(p => `${p} = :${p}`).join(' AND ')}`
      var conn = await oracleConnection.getConnection()
      var mapData = {}
      for (let i in primary) {
        mapData[i] = { val: primary[i] }
      }
      try {
        const result = await conn.execute(sql, mapData, { autoCommit: true })
        return result
      } catch(e) {
        throw e
      } finally {
        if (conn) {
          conn.close().catch((err) => {})
          console.log('conn has close')
        }
      }
    },
    pagination: async (sql, options) => {
      const length = options.length || 10
      const page = options.page || 1
      // const begin = ((page-1) * length) + 1
      // const offset = page*length

      // const itemSql = `SELECT * FROM (${sql}) WHERE line_number BETWEEN ${begin} AND ${offset}  ORDER BY line_number`
      // const countSql = `SELECT count(*) as total_record FROM (${sql})`

      const items = await query(sql)
      const total_count = items.length
      const last_page = Math.ceil(total_count/length)
      return {
        page,
        length,
        last_page,
        total_count,
        items: items.slice((page-1)*length, (page*length))
      }
    },
    parseFilter: (filters) => {
      const filtersCommand = Object.keys(filters).length > 0 ? `WHERE ${Object.keys(filters).map(prop => {
        if (Array.isArray(filters[prop]) && filters[prop].length > 0) { // case filter multi value string
          return `${prop} IN (${filters[prop].map(valArr => `'${valArr}'`).join(', ')})` 
        } 
        if (Array.isArray(filters[prop]) && filters[prop].length > 0 && prop[0] === '!') { // case filter multi value string
          prop = prop.substr(1)
          return `${prop} NOT IN (${filters[prop].map(valArr => `'${valArr}'`).join(', ')})` 
        } else if (typeof filters[prop] === 'object') { // case fitler date range
          if (filters[prop].from && filters[prop].to) {
            return `${prop} BETWEEN TO_DATE('${filters[prop].from}', 'YYYY-MM-DD') AND TO_DATE('${filters[prop].to}', 'YYYY-MM-DD')` 
          } else if (filters[prop].from) {
            return `${prop} > TO_DATE('${filters[prop].from}', 'YYYY-MM-DD')` 
          } else if (filters[prop].to) {
            return `${prop} < TO_DATE('${filters[prop].to}', 'YYYY-MM-DD')` 
          }
        } else if (filters[prop][0] === '!') {
          return `${prop} NOT '${filters[prop].substring(1)}'`
        } else { // case filter single value string
          return `${prop} LIKE '%${filters[prop]}%'`
        }
      }).join(' AND ')}` : ''
      return filtersCommand
    }
  }
}
