
const oracleSql = require('./oracle.sql')
const { promisify } = require('util')
const bcrypt = require('bcrypt')

const hash = promisify(bcrypt.hash)

module.exports = (oracleConnection) => {
  const { query, update } = oracleSql(oracleConnection)

  return {
    getConfig: async (id) => {
      const sql = `SELECT
        data
      FROM ots_config
      WHERE id = '${id}'`
      const response = await query(sql)
      return response[0].DATA
    },
    updateConfig: async (ID, DATA) => {
      return update('ots_config', { ID }, { DATA })
    }
  }
}