const oracleSql = require('./oracle.sql')

module.exports = (oracleConnection) => {
  const { query, upsert, remove, pagination, parseFilter } = oracleSql(oracleConnection)

  return {
    getAdminDocumentListHY: async (options) => {
      const sort = {
        name: options.sort_name || 'SO',
        by: options.sort_by || 'DESC'
      }
      
      const filters = {}
      if (options.DO_NO && options.DO_NO != '') {
        filters['dhy.DO'] = options.DO_NO
      }

      if (options.SO_NO && options.SO_NO != '') {
        filters['dhy.SO'] = options.SO_NO
      }

      if (options.SHIP_DATE && options.SHIP_DATE != '') {
        filters['dhy.PLAN_GL_SHIP_DATE'] = {
          from: options.SHIP_DATE,
          to: options.SHIP_DATE
        }
      }

      if (options.PAYMENT_TERM && options.PAYMENT_TERM != '') {
        filters['dhy.PAYMENT_TERM'] = options.PAYMENT_TERM
      }

      if (options.COMMERCIAL_INVOICE && options.COMMERCIAL_INVOICE != '') {
        filters['dhy.COMMERCIAL_INVOICE'] = options.COMMERCIAL_INVOICE
      }

      if (options.DESTINATION_PORT && options.DESTINATION_PORT != '') {
        filters['dhy.DESTINATION_PORT'] = options.DESTINATION_PORT
      }

      if (options.ETD && options.ETD != '') {
        filters['dhy.ETD'] = options.ETD
      }

      if (options.ETA && options.ETA != '') {
        filters['dhy.ETA'] = options.ETA
      }

      if (options.STATUS && options.STATUS != '') {
        filters['dhy.STATUS'] = options.STATUS
      }

      const filtersCommand = parseFilter(filters)

      const sql = `
        SELECT
          dhy.*,
          row_number() over (ORDER BY dhy.${sort.name} ${sort.by}) line_number
        FROM ots_document_hy dhy ${filtersCommand}`
      return await pagination(sql, options)
    },
    getDocumentDetailHY: async (so, do_no, item) => {
      const sql = `
        SELECT 
          dhy.*,
          vt.pi_no,
          vt.po_no,
          vt.customer_code
        FROM ots_document_hy dhy 
        LEFT JOIN  v_spt_truck_tms vt ON dhy.so = vt.so_no_ref AND dhy.do = vt.pi_no AND dhy.item = vt.so_item
        WHERE dhy.so = '${so}' AND dhy.do = '${do_no}' AND dhy.item = '${item}'`
      return await query(sql)
    },
    upsertDocumentHY: async (primary, data) => {
      return await upsert('ots_document_hy', primary, data)
    },
    removeDocumentHy: async (primary) => {
      return await remove('ots_document_hy', primary)
    },
    getAdminDocumentHYDashboard: async () => {
      const sql = `SELECT
        SUM (
          CASE status
            WHEN 'completed' THEN 1
            ELSE 0
          END
        ) AS COMPLETED,
        SUM (
          CASE status
            WHEN 'pending' THEN 1
            ELSE 0
          END
        ) AS PENDING
        FROM ots_document_hy`
      return query(sql)
    }
  }
}