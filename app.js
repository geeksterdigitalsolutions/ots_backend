'use strict'

const path = require('path')
const AutoLoad = require('fastify-autoload')
const TNSName = `(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=${process.env.ORACLE_HOST})(PORT=${process.env.ORACLE_PORT}))(CONNECT_DATA=(SERVER=DEDICATED)(SID = ${process.env.ORACLE_SID})))`
console.log(TNSName)

module.exports = async (fastify, opts) => {
  // Place here your custom code!

  // Do not touch the following lines
  fastify.register(require('fastify-file-upload'))
  fastify.register(require('fastify-sensible'))
  fastify.register(require('fastify-cookie'))
  fastify.register(require('fastify-cors'), { 
    origin: '*',
    methods: ['GET', 'POST', 'PATCH', 'PUT', 'DELETE', 'OPTIONS'],
    allowedHeaders: ['Content-Type', 'Authorization']
  })
  fastify.register(require('fastify-oracle'), {
    pool: {
      user: process.env.ORACLE_USERNAME,
      password: process.env.ORACLE_PASSWORD,
      connectString: TNSName
    }
  })
  fastify.register(require('fastify-server-timeout'), {
    serverTimeout: 10000000 //ms
  })

  fastify.register(require('fastify-jwt'), {
    secret: 'Ot$Geek$t3r'
  })
  
  // This loads all plugins defined in plugins
  // those should be support plugins that are reused
  // through your application
  fastify.register(AutoLoad, {
    dir: path.join(__dirname, 'plugins'),
    options: Object.assign({}, opts)
  })

  // This loads all plugins defined in services
  // define your routes in one of these
  fastify.register(AutoLoad, {
    dir: path.join(__dirname, 'routes'),
    options: opts
  })

  fastify.ready((err) => {
    if (err) {
      console.log(err)
      process.exit()
    }
    console.log(fastify.printRoutes())
  })
}
