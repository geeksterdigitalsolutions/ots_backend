#!/usr/bin/env bash

APP="geekster/ots_backend"
docker build -t ${APP} .
docker push ${APP}